/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.common;

public class HeliosConstants {
    
    public static int MOVE_FORWARD_CMD = 1; 
    
    public static int MOVE_REVERSE_CMD = 2;
    
    public static int MOVE_LEFT_CMD = 3;
    
    public static int MOVE_RIGHT_CMD = 4;
    
    public static int UPLINK_MESS_TYPE = 5;
    
    public static int DOWNLINK_MESS_TYPE = 6;
    
}
