/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.message;

import edu.helios.exception.HeliosException;
import java.util.Vector;


public class UplinkMessage extends HeliosMessage{
    
    private int value;
    
    private int length;
    
    private Vector route = null;
    
    public UplinkMessage(int type, int seqNumber, int value) throws HeliosException{
        
        super(type,seqNumber);
        this.value = value;
    }
    
    public void appendToRoute(int nodeId){
        
        Integer node = new Integer(nodeId);
        if(this.route == null){
            this.route = new Vector();
        }
        route.addElement(node);
        this.length++;
        
    }

    public int getLength() {
        return length;
    }

    public Vector getRoute() {
        return route;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        
        String superString = super.toString();
        return superString+ ", value=" + value + ", length=" + length + ", route=" + route;
    }
        
    
}
