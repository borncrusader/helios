/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.message;

import edu.helios.common.HeliosConstants;
import edu.helios.exception.HeliosException;

public class HeliosMessage {
    
    private int type;

    private int seqNumber;
    
    public HeliosMessage(int messageType, int sequenceNumber) throws HeliosException{
        
        if((messageType != HeliosConstants.DOWNLINK_MESS_TYPE) 
                && messageType != (HeliosConstants.UPLINK_MESS_TYPE)){
            throw new HeliosException("Invalid message type " + messageType + 
                    " passed to the HeliosMessage constructor");
        }
        
        if(sequenceNumber < 0){
            throw new HeliosException("Invalid seqence number " + sequenceNumber + 
                    " passed to the HeliosMessage constructor");
        }
        
        this.type = messageType;
        this.seqNumber = sequenceNumber;
    }
    
    public int getType(){
        
        return this.type;
    }
    
    public int getSeqNumber(){
        
        return this.seqNumber;
    }
    
    public String toString(){
        
        String messType = (type == HeliosConstants.DOWNLINK_MESS_TYPE?"Downlink Message":"Uplink Message");
        
        return messType + ", Seq No = " + this.seqNumber; 
    }

    public boolean equals(Object obj) {
        
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HeliosMessage other = (HeliosMessage) obj;
        if (this.type != other.type) {
            return false;
        }
        if (this.seqNumber != other.seqNumber) {
            return false;
        }
        
        return true;
    }
    
    public int hashCode(){
        
        return this.type + this.seqNumber;
    }
}
