/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.message;

import edu.helios.common.RobotCommand;
import edu.helios.exception.HeliosException;


public class DownlinkMessage extends HeliosMessage{
    
    private RobotCommand command = null;
    
    public DownlinkMessage(int type, int sequenceNumber,int command, int duration) throws HeliosException{
        
        super(type,sequenceNumber);
        this.command = new RobotCommand(command, duration);
        
    }

    public String toString() {
        
        String superString = super.toString();
        return superString + ", command=" + command;
    }

    
    
    public RobotCommand getCommand() {
        return command;
    }
       
    
}
