package edu.helios.forwarder;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import com.sun.spot.core.peripheral.Spot;
import com.sun.spot.core.resources.Resources;
import com.sun.spot.core.resources.transducers.ITriColorLED;
import com.sun.spot.core.util.Utils;
import com.sun.spot.espot.peripheral.ESpot;
import com.sun.spot.ieee_802_15_4_radio.IRadioPolicyManager;
import com.sun.spot.multihop.io.j2me.radiogram.RadiogramConnection;
import edu.helios.common.HeliosConstants;
import edu.helios.message.HeliosMessage;
import edu.helios.message.UplinkMessage;
import edu.helios.utils.MessageUtils;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class ForwarderMidlet extends MIDlet{
    
    /*
     * This class is the midlet that is run on the forwarding (router) 
     * sun spots. It starts a thread which listens for the beacon 
     * request message. It adds itself ot the route list and 
     * rebroadcasts it.
     */
    
    protected void startApp() throws MIDletStateChangeException {
        
        /*
         * Start the listening on the port 80
         */
        ForwarderThread forwarder = new ForwarderThread(HeliosConstants.BEACON_PORT_NUMBER);
        Thread thread1 = new Thread(forwarder);
        thread1.start();
        
    }
    
    protected void pauseApp() {
        // This will never be called by the Squawk VM
    }
    
    protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
        // Only called if startApp throws any exception other than MIDletStateChangeException
    }
    
}
