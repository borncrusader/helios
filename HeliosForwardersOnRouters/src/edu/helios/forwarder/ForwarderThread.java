package edu.helios.forwarder;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
import com.sun.spot.core.peripheral.Spot;
import com.sun.spot.core.resources.Resources;
import com.sun.spot.core.resources.transducers.ITriColorLED;
import com.sun.spot.core.util.Utils;
import com.sun.spot.espot.peripheral.ESpot;
import com.sun.spot.ieee_802_15_4_radio.IRadioPolicyManager;
import com.sun.spot.multihop.io.j2me.radiogram.RadiogramConnection;
import edu.helios.common.HeliosConstants;
import edu.helios.message.BeaconRequestMessage;
import edu.helios.message.BeaconResponseMessage;
import edu.helios.message.HeliosMessage;
import edu.helios.message.UplinkMessage;
import edu.helios.utils.BeaconMessageUtils;
import edu.helios.utils.MessageUtils;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class ForwarderThread implements Runnable {

    /*
     * This is the thread that listens on port 80 and 
     * receives the beacon request broadcast message.
     * It then appends its ID to the list of route's
     * in the packet and re-broadcasts it. Thus all 
     * forwarders add their ID's in the route of the
     * beacon message.
     */
    
    private int selfId = 0;
    private RadiogramConnection radioConnectionBroadcast = null;
    private RadiogramConnection radioConnectionReceive = null;
    private Datagram sendDatagram = null;
    private Datagram receiveDatagram = null;
    private int operationPort = 0;
    private int beaconReqSequenceNumber = 0;
    private int beaconRespSequenceNumber = 0;

    public ForwarderThread(int operationPort) {

        this.operationPort = operationPort;

    }

    /*
     * Get the int representation of the last 4
     * digits of the IEEE MAC address and set it
     * as the ID.
     */
    private void setMyId() {

        int index = 0;
        String lastFourDigits = "";
        String ourAddress = System.getProperty("IEEE_ADDRESS");
        index = ourAddress.lastIndexOf('.');
        lastFourDigits = ourAddress.substring(index + 1);
        selfId = Integer.parseInt(lastFourDigits, 16);

    }

    public void run() {
        HeliosMessage recvdMessage = null;
        BeaconMessageUtils messUtil = new BeaconMessageUtils();
        setMyId();
        String ourAddress = System.getProperty("IEEE_ADDRESS");
        ITriColorLED led = (ITriColorLED) Resources.lookup(ITriColorLED.class, "LED7");
        System.out.println("From " + this.operationPort + " : Starting sensor sampler application on " + ourAddress + " ...");
        IRadioPolicyManager rpm = ((ESpot) Spot.getInstance()).getRadioPolicyManager();
        rpm.setOutputPower(HeliosConstants.OUTPUT_POWER);

        try {
            /*
             * Initialize the connection
             */
            radioConnectionBroadcast = (RadiogramConnection) Connector.open("radiogram://broadcast:" + this.operationPort);
            sendDatagram = radioConnectionBroadcast.newDatagram(HeliosConstants.MAX_DATAGRAM_LENGTH);
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            radioConnectionReceive = (RadiogramConnection) Connector.open("radiogram://:" + this.operationPort);
            receiveDatagram = radioConnectionReceive.newDatagram(radioConnectionReceive.getMaximumLength());
            // Open up a broadcast connection to the host port
            // where the 'on Desktop' portion of this demo is listening
        } catch (Exception e) {
            System.err.println("From " + this.operationPort + " :Caught " + e + " in connection initialization.");
            //notifyDestroyed();
        }

        while (true) {
            try {

                led.setRGB(0, 255, 0);
                led.setOn();
                /*
                 * Wait for beacon requests
                 */
                System.out.println("From " + this.operationPort + " :Waiting for messages.....");

                radioConnectionReceive.receive(receiveDatagram);
                /*
                 * Decode the packet
                 */
                recvdMessage = messUtil.decodePacket(receiveDatagram);
                System.out.println("From " + this.operationPort + " :Received the packet : " + recvdMessage);
                
                if (recvdMessage.getType() == HeliosConstants.BEACON_REQUEST_TYPE) {
                    if (this.beaconReqSequenceNumber < recvdMessage.getSeqNumber()) {
                        /*
                         * Got a beacon request.
                         */
                        System.out.println("From " + this.operationPort + " :Adding myself " + selfId + " to the route of the mess : "+recvdMessage);
                        /*
                         * Append my MAC ID (int representation of last 4 digits of the MAC addrress)
                         * and rebroadcast the message
                         */
                        ((BeaconRequestMessage) recvdMessage).appendToRoute(selfId);
                        this.beaconReqSequenceNumber = recvdMessage.getSeqNumber();
                    }else{
                        /*
                         * Received a duplicate message. Discard and do nothing.
                         */
                        System.out.println("Already seen the message. DOing nothing. Mess : "+recvdMessage);
                        receiveDatagram.reset();
                        led.setRGB(255, 0, 0);
                        led.setOn();
                        Utils.sleep(100);
                        led.setOff();
                        continue;
                    }
                } else if (recvdMessage.getType() == HeliosConstants.BEACON_RESPONSE_TYPE) {
                    if (this.beaconRespSequenceNumber < recvdMessage.getSeqNumber()) {
                        System.out.println("From " + this.operationPort + " :Adding myself " + selfId + " to the route");
                        //((BeaconResponseMessage) recvdMessage).appendToRoute(selfId);
                        this.beaconRespSequenceNumber = recvdMessage.getSeqNumber();
                    }else{
                        /*
                         * Received a duplicate message. Discard and do nothing.
                         */
                        System.out.println("Already seen the message. DOing nothing. Mess : "+recvdMessage);
                        receiveDatagram.reset();
                        led.setRGB(255, 0, 0);
                        led.setOn();
                        Utils.sleep(100);
                        led.setOff();
                        continue;
                    }
                }else{
                    /*
                     * Received an unexpected message type. Discard and do nothing.
                     */
                    System.out.println("Received an unknown mess. Doing nothing. Mess : "+recvdMessage);
                    receiveDatagram.reset();
                    led.setRGB(255, 0, 0);
                    led.setOn();
                    Utils.sleep(100);
                    led.setOff();
                    continue;
                }

                // Flash an LED to indicate a sampling event
                led.setRGB(255, 255, 255);
                led.setOn();
                Utils.sleep(100);
                led.setOff();

                /*
                 * Encode the packet and re-broadcast it.
                 */
                messUtil.encodePacket(recvdMessage, sendDatagram);

                System.out.println("From " + this.operationPort + " :Forwarding the message : " + recvdMessage);

                radioConnectionBroadcast.send(sendDatagram);
                sendDatagram.reset();
                receiveDatagram.reset();
            } catch (Exception e) {
                System.err.println("From " + this.operationPort + " :Caught " + e + " while collecting/sending sensor sample.");
            }
        }
    }
}
