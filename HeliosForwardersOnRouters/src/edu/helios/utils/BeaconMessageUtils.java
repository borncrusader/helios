/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.utils;

import edu.helios.common.HeliosConstants;
import edu.helios.exception.HeliosException;
import edu.helios.message.*;
import java.io.IOException;
import javax.microedition.io.Datagram;

/**
 * This is an utility class used to encode and decode
 * Beacon messages.
 * 
 */
public class BeaconMessageUtils {

    private void encodeBeaconRequestPacket(BeaconRequestMessage message, Datagram dataGram) throws IOException, HeliosException {

        int length = 0;
        int index = 0;
        if (message == null) {
            throw new HeliosException("The message sent is null");
        }
        if (dataGram == null) {
            throw new HeliosException("The data gram passed is null");
        }
        dataGram.writeInt(message.getType());
        dataGram.writeInt(message.getLength());
        dataGram.writeInt(message.getSeqNumber());
        if (message.getLength() > 0) {
            length = message.getLength();
            while (index < length) {
                dataGram.writeInt(((Integer) message.getRoute().elementAt(index)).intValue());
                index++;
            }
        }

    }

    private void encodeBeaconResponsePacket(BeaconResponseMessage message, Datagram dataGram) throws IOException, HeliosException {

        int length = 0;
        int index = 0;
        if (message == null) {
            throw new HeliosException("The message sent is null");
        }
        if (dataGram == null) {
            throw new HeliosException("The data gram passed is null");
        }
        dataGram.writeInt(message.getType());
        dataGram.writeInt(message.getLength());
        dataGram.writeInt(message.getSeqNumber());
        if (message.getLength() > 0) {
            length = message.getLength();
            while (index < length) {
                dataGram.writeInt(((Integer) message.getRoute().elementAt(index)).intValue());
                index++;
            }
        }

    }

    public void encodePacket(HeliosMessage message, Datagram dataGram) throws HeliosException {

        BeaconRequestMessage beaconReqMess = null;
        BeaconResponseMessage beaconRespMess = null;
        if (message == null) {
            throw new HeliosException("The message sent is null");
        }
        if (dataGram == null) {
            throw new HeliosException("The datagram passed is null");
        }
        if (message.getType() == HeliosConstants.BEACON_REQUEST_TYPE) {
            beaconReqMess = (BeaconRequestMessage) message;
            try {
                encodeBeaconRequestPacket(beaconReqMess, dataGram);
            } catch (IOException ex) {
                throw new HeliosException("Caught an IOException while forming the downlink packet '"
                        + message + "' : " + ex.getMessage());
            }
        } else if (message.getType() == HeliosConstants.BEACON_RESPONSE_TYPE) {
            beaconRespMess = (BeaconResponseMessage) message;
            try {
                encodeBeaconResponsePacket(beaconRespMess, dataGram);
            } catch (IOException ex) {
                throw new HeliosException("Caught an IOException while forming the uplink packet '"
                        + message + "' : " + ex.getMessage());
            }
        } else {
            throw new HeliosException("Unknown message type " + message + " passed");
        }

    }

    private BeaconRequestMessage decodeBeaconRequestMessage(Datagram datagram) throws IOException, HeliosException {

        BeaconRequestMessage message = null;
        int length = 0;
        int sequenceNumber = 0;
        int routeId = 0;

        if (datagram == null) {
            throw new HeliosException("The datagram passed is null");
        }

        length = datagram.readInt();
        sequenceNumber = datagram.readInt();
        message = new BeaconRequestMessage(sequenceNumber);
        if (length > 0) {
            while (length > 0) {
                routeId = datagram.readInt();
                message.appendToRoute(routeId);
                length--;
            }
        }

        return message;

    }

    private BeaconResponseMessage decodeBeaconResponseMessage(Datagram datagram) throws IOException, HeliosException {

        BeaconResponseMessage message = null;
        int length = 0;
        int sequenceNumber = 0;
        int routeId = 0;

        if (datagram == null) {
            throw new HeliosException("The datagram passed is null");
        }

        length = datagram.readInt();
        sequenceNumber = datagram.readInt();
        message = new BeaconResponseMessage(sequenceNumber);
        if (length > 0) {
            while (length > 0) {
                routeId = datagram.readInt();
                message.appendToRoute(routeId);
                length--;
            }
        }

        return message;

    }

    public HeliosMessage decodePacket(Datagram dataGram) throws HeliosException {

        HeliosMessage message = null;
        int type = 0;

        if (dataGram == null) {
            throw new HeliosException("The datagram passed is null");
        }
        try {
            type = dataGram.readInt();
            if (type == HeliosConstants.BEACON_REQUEST_TYPE) {
                message = decodeBeaconRequestMessage(dataGram);
            } else if (type == HeliosConstants.BEACON_RESPONSE_TYPE) {
                message = decodeBeaconResponseMessage(dataGram);
            } else {
                throw new HeliosException("Unknown message type " + type + " present in the passed datagram");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new HeliosException("Caught an IOException while decoding the datagram : " + ex.getMessage());
        }

        return message;
    }
}
