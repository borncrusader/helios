/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.utils;

import edu.helios.common.HeliosConstants;
import edu.helios.common.RobotCommand;
import edu.helios.exception.HeliosException;
import edu.helios.message.DownlinkMessage;
import edu.helios.message.UplinkMessage;
import edu.helios.message.HeliosMessage;
import java.io.IOException;
import javax.microedition.io.Datagram;

/**
 * This is an utility class used to encode and decode
 * uplink and downlink messages.
 * 
 */
public class MessageUtils {
    
    
    private UplinkMessage decodeUplinkMessage(Datagram datagram) throws IOException, HeliosException{
        
        UplinkMessage message = null;
        int length = 0;
        int sequenceNumber = 0;
        int value = 0;
        int routeId = 0;
        
        if(datagram == null){
            throw new HeliosException("The datagram passed is null");
        }
        
        length = datagram.readInt();
        sequenceNumber =datagram.readInt();
        value = datagram.readInt();
        message = new UplinkMessage(sequenceNumber, value);
        if(length > 0){
            while(length > 0){
                routeId = datagram.readInt();
                message.appendToRoute(routeId);
                length--;
            }
        }
        
        return message;
        
    }
    
    private DownlinkMessage decodeDownLinkMessage(Datagram datagram) throws IOException, HeliosException{
        
        DownlinkMessage message = null;
        int seqNumber = 0;
        int command = 0;
        int duration = 0;
        
        if(datagram == null){
            throw new HeliosException("The datagram passed is null");
        }
        seqNumber = datagram.readInt();
        command = datagram.readInt();
        duration = datagram.readInt();
        
        RobotCommand robotCommand = new RobotCommand(command, duration);
        message = new DownlinkMessage(seqNumber, robotCommand);
        
        return message;
        
    }
    
    private void encodeDownlinkPacket(DownlinkMessage message, Datagram dataGram) throws IOException, HeliosException{
        
        if(message == null){
            throw new HeliosException("The message sent is null");
        }
        if(dataGram == null){
            throw new HeliosException("The datagram passed is null");
        }
        dataGram.writeInt(message.getType());
        dataGram.writeInt(message.getSeqNumber());
        dataGram.writeInt(message.getCommand().getCommandType());
        dataGram.writeInt(message.getCommand().getDuration());
        
    }

    private void encodeUplinkPacket(UplinkMessage message, Datagram dataGram) throws IOException, HeliosException{
        
        int length = 0;
        int index = 0;
        if(message == null){
            throw new HeliosException("The message sent is null");
        }
        if(dataGram == null){
            throw new HeliosException("The data gram passed is null");
        }
        dataGram.writeInt(message.getType());
        dataGram.writeInt(message.getLength());
        dataGram.writeInt(message.getSeqNumber());
        dataGram.writeInt(message.getValue());
        if(message.getLength() > 0){
            length = message.getLength();
             while(index < length){
                dataGram.writeInt(((Integer)message.getRoute().elementAt(index)).intValue());
                index++;
            }
        }
        
    }

    
    public void encodePacket(HeliosMessage message, Datagram dataGram) throws HeliosException{
        
        UplinkMessage uplinkMessage = null;
        DownlinkMessage downlinkMessage = null;
        if(message == null){
            throw new HeliosException("The message sent is null");
        }
        if(dataGram == null){
            throw new HeliosException("The datagram passed is null");
        }
        if(message.getType() == HeliosConstants.DOWNLINK_MESS_TYPE){
            downlinkMessage = (DownlinkMessage)message;
            try {
                encodeDownlinkPacket(downlinkMessage, dataGram);
            } catch (IOException ex) {
                throw new HeliosException("Caught an IOException while forming the downlink packet '"
                        +message+"' : "+ex.getMessage());
            }
        }else if(message.getType() == HeliosConstants.UPLINK_MESS_TYPE){
            uplinkMessage = (UplinkMessage)message;
            try {
                encodeUplinkPacket(uplinkMessage, dataGram);
            } catch (IOException ex) {
                throw new HeliosException("Caught an IOException while forming the uplink packet '"
                        +message+"' : "+ex.getMessage());
            }
        }else{
            throw new HeliosException("Unknown message type "+message+" passed");
        }
        
    }

    public HeliosMessage decodePacket(Datagram dataGram) throws HeliosException{
        
        HeliosMessage message = null;
        int type = 0;
        
        if(dataGram == null){
            throw new HeliosException("The datagram passed is null");
        }
        try {
            type = dataGram.readInt();
            if(type == HeliosConstants.DOWNLINK_MESS_TYPE){
                message = decodeDownLinkMessage(dataGram);
            }else if(type == HeliosConstants.UPLINK_MESS_TYPE){
                message = decodeUplinkMessage(dataGram);
            }else{
                throw new HeliosException("Unknown message type "+type+" present in the passed datagram");
            }
        } catch (IOException ex) {
            throw new HeliosException("Caught an IOException while decoding the datagram : "+ex.getMessage());
        }
        
        return message;
    }
    
}


