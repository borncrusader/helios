/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.exception;

/*
 * This class represents a custom exception for this
 * application
 */

public class HeliosException extends Exception{
    
    public HeliosException(String message){
        
        super(message);
    }
    
}
