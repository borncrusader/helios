/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.common;

public class HeliosConstants {
    
    /*
     * The sample period for the light sensing. Used only for testing.
     */
    public static int SAMPLE_PERIOD = 10 * 1000;

    /*
     * The port on which the uplink message is sent to the base station
     */
    public static int SEND_PORT = 73;
    
    /*
     * The port on which the sun spot mounted on the robot listens for 
     * downlink messages from base station
     */
    public static int RECEIVE_PORT = 73;
    
    /*
     * The integer representation of the move forward command
     */
    public static int MOVE_FORWARD_CMD = 1; 
    
    /*
     * The integer representation of the move reverse command
     */
    public static int MOVE_REVERSE_CMD = 2;
    
    /*
     * The integer representation of the move left command
     */
    public static int MOVE_LEFT_CMD = 3;
    
    /*
     * The integer representation of the move right command
     */
    public static int MOVE_RIGHT_CMD = 4;
    
    /*
     * The message type of Uplink message from the robot to
     * base station
     */
    public static int UPLINK_MESS_TYPE = 5;
    
    /*
     * The message type of Uplink message from the robot to
     * base station
     */
    public static int DOWNLINK_MESS_TYPE = 6;
    
    /*
     * The maximum datagram length that is sent by the application
     */
    public static int MAX_DATAGRAM_LENGTH = 100;
    
    /*
     * The output power that is set on this spot
     */
    public static int OUTPUT_POWER = -27;
    
    /*
     * The port used to communicate with robot1
     */
    public static int ROBOT_PORT1 = 73;
    
    /*
     * The port used to communicate with robot2
     */
    public static int ROBOT_PORT2 = 74;
    
    /*
     * Message type for the beacon request message
     */
    public static int BEACON_REQUEST_TYPE = 50;
            
    /*
     * Message type for the beacon response message
     */
    public static int BEACON_RESPONSE_TYPE = 100;        
    
    /*
     * The time out period for the beacon transmit messages
     */
    public static int BEACON_RETRANSMIT_TIMEOUT_PERIOD = 5000;
    
    /*
     * The port on which the beacon is sent and received
     */
    public static int BEACON_PORT_NUMBER = 80;
}
