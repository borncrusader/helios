/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.message;

import edu.helios.common.HeliosConstants;
import edu.helios.exception.HeliosException;
import java.util.Vector;

/**
 * This is a class that represents a uplink message from 
 * the sun spot mounted on the robot to the base station.
 * The light value represents the light value that is 
 * recorded by the sun spot mounted on robot.
 */
public class UplinkMessage extends HeliosMessage{
    
    private int value;
    
    private int length;
    
    private Vector route = null;
    
    public UplinkMessage(int seqNumber, int value) throws HeliosException{
        
        super(HeliosConstants.UPLINK_MESS_TYPE, seqNumber);
        this.value = value;
    }
    
    public void appendToRoute(int nodeId){
        
        Integer node = new Integer(nodeId);
        if(this.route == null){
            this.route = new Vector();
        }
        route.addElement(node);
        this.length++;
        
    }

    public int getLength() {
        return length;
    }

    public Vector getRoute() {
        return route;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        
        String superString = super.toString();
        return superString+ ", value=" + value + ", length=" + length + ", route=" + route;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setRoute(Vector route) {
        this.route = route;
    }

    public void setValue(int value) {
        this.value = value;
    }
    
    
}
