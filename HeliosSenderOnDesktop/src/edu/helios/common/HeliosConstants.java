/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.common;

public class HeliosConstants {
    
    /*
     * The IEEE MAC address of the base station
     */
    public static String BASE_STATION_ADDRESS = "0014.4F01.0000.0761";
       
    /*
     * The IEEE MAC address of the sun spotmountedon robot1
     */
    public static String ROBOT1_ADDRESS = "0014.4F01.0000.04A8"; // D32
    
    /*
     * The IEEE MAC address of the sun spotmountedon robot2
     */
    public static String ROBOT2_ADDRESS = "0014.4F01.0000.0A13"; // D24
    
    /*
     * The port used to communicate with robot1
     */
    public static int ROBOT_PORT1 = 73;
    
    /*
     * The port used to communicate with robot2
     */
    public static int ROBOT_PORT2 = 74;
    
    /*
     * The integer representation of the move forward command
     */
    public static int MOVE_FORWARD_CMD = 1; 
    
    /*
     * The integer representation of the move reverse command
     */
    public static int MOVE_REVERSE_CMD = 2;
    
    /*
     * The integer representation of the move left command
     */
    public static int MOVE_LEFT_CMD = 3;
    
    /*
     * The integer representation of the move right command
     */
    public static int MOVE_RIGHT_CMD = 4;
    
    /*
     * The message type of Uplink message from the robot to
     * base station
     */
    public static int UPLINK_MESS_TYPE = 5;
    
     /*
     * The message type of Downlink message from the robot to
     * base station
     */
    public static int DOWNLINK_MESS_TYPE = 6;
    
    /*
     * The maximum datagram length that is sent by the application
     */
    public static int MAX_DATAGRAM_LENGTH = 100;
    
    /*
     * The retransmit time out period for the sender
     */
    public static int RETRANSMIT_TIMEOUT_PERIOD = 1000;
    
    /*
     * Number of times the sender will retransmit before giving up
     */
    public static int RETRANSMIT_LIMIT = 10000;
    
    /*
     * Indicator that is used to denote that the light value 
     * is decreasing
     */
    public static int LIGHT_VALUE_DECREASING_INDICATOR = 1;
   
    /*
     * Indicator that is used to denote that the light value 
     * is increasing
     */
    public static int LIGHT_VALUE_INCREASING_INDICATOR = 2; 
    
    /*
     * Indicator that is used to denote that the light 
     * threshold value is reazched
     */
    public static int LIGHT_THRESHOLD_VALUE_REACHED_INDICATOR = 3; 
    
    /*
     * Indicator that is used to denote that the light value 
     * is zero in all directions after a scan
     */
    public static int LIGHT_ZERO_IN_SCAN = 0;
    
    /*
     * Indicator that is used to denote that the light value 
     * is non zero in at least one of the directions after
     * scan
     */
    public static int LIGHT_NON_ZERO_IN_SCAN = 1;
    /*
     * The light value that decides when the bot should stop.
     * When the light value is received, the bot will stop.
     */
    public static int LIGHT_SOURCE_SENSED_THRESHOLD = 200;
    
    /*
     * The maximum number of received messages that are needed to 
     * be maintained by the host. It has to be atleast equal to
     * VIOLATION_CHECK_THRESHOLD_RECORDS
     */
    public static int RECEIVE_RECORD_COUNT_LIMIT = 50;
    
    /*
     * The distance covered by the robot every time it moves
     * ahead. The value is in cms
     */
    public static int DISTANCE_PER_STRAIGHT_MOVEMENT = 20;
    
    /*
     * The angle with which the robot turns while scanning for
     * light. It takes one reading per angle and reports it to
     * the BS.
     */
    public static int ANGLE_COVERED_PER_TURN = 45;
    
    /*
     * Number of readings for which the light value has to be
     * decreasing for it to be considered a violation. After a
     * violation has been declared, the robot enters a scan mode
     */
    public static int VIOLATION_CHECK_THRESHOLD_RECORDS = 2;
    
    /*
     * Number of times the same seq is received before topology is broken
     */
    public static int TOPOLOGY_TIMEOUT_THRESHOLD = 5;
    
    /*
     * Message type for the beacon request message
     */
    public static int BEACON_REQUEST_TYPE = 50;
       
    /*
     * Message type for the beacon response message
     */
    public static int BEACON_RESPONSE_TYPE = 100;        
    
    /*
     * The time out period for the beacon transmit messages
     */
    public static int BEACON_RETRANSMIT_TIMEOUT_PERIOD = 5000;
    
    /*
     * The port on which the beacon is sent and received
     */
    public static int BEACON_PORT_NUMBER = 80;
    
}
