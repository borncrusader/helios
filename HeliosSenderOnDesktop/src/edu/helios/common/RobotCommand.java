/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.common;

import edu.helios.exception.HeliosException;

/**
 * This is a class that represents the command that is 
 * sent to the robot. The command type field defines the 
 * type of the command sent and duration is the time in 
 * milliseconds for which the robot should execute the 
 * command. 
 */

public class RobotCommand {
    
    private int commandType;
    
    private int duration;
    
    public RobotCommand(int command, int duration) throws HeliosException{
        
       if((command != HeliosConstants.MOVE_FORWARD_CMD) 
               && (command != HeliosConstants.MOVE_LEFT_CMD) 
               && (command != HeliosConstants.MOVE_RIGHT_CMD) 
               && (command != HeliosConstants.MOVE_REVERSE_CMD)){
           throw new HeliosException("Invalid command type "+ command 
                   + " passed to the constructor of RobotCommand");
       }
       if(duration < 0){
             throw new HeliosException("Invalid duration "+ duration 
                   + " passed to the constructor of RobotCommand");
       }
       this.commandType = command;
       this.duration = duration;
    }

    public int getCommandType() {
        return commandType;
    }

    public int getDuration() {
        return duration;
    }

    public String toString() {
    
        String commandString = "";
        if(commandType == HeliosConstants.MOVE_FORWARD_CMD){
            commandString = "FORWARD";
        }else if(commandType == HeliosConstants.MOVE_LEFT_CMD){
            commandString = "LEFT";
        }else if(commandType == HeliosConstants.MOVE_RIGHT_CMD){
            commandString = "RIGHT";
        }else if(commandType == HeliosConstants.MOVE_REVERSE_CMD){
            commandString = "REVERSE";
        }
        
        return "RobotCommand is 'move "+commandString+" for the duration =" + duration;
    }

    
    
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RobotCommand other = (RobotCommand) obj;
        if (this.commandType != other.commandType) {
            return false;
        }
        if (this.duration != other.duration) {
            return false;
        }
        return true;
    }

    public int hashCode() {
    
        return this.commandType + this.duration;
    }
       
    
    
}
