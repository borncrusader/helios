/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.sender;

import com.sun.spot.multihop.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.multihop.radio.NoRouteException;
import edu.helios.exception.HeliosException;
import edu.helios.message.BeaconRequestMessage;
import edu.helios.message.HeliosMessage;
import edu.helios.utils.BeaconMessageUtils;
import java.io.IOException;
import java.util.TimerTask;
import javax.microedition.io.Datagram;

/**
 * This thread broadcasts the beacon request message passed
 * 
 */
public class BeaconRetransmitThread extends TimerTask{

    BeaconMessageUtils beaconMesssUtils = new BeaconMessageUtils();
    
    RadiogramConnection radioConnection = null;
    
    Datagram dataToSend = null;
    
    int beaconSeqNumber = 1;
    
    public BeaconRetransmitThread(RadiogramConnection radioConn, Datagram dataToSend) throws HeliosException{
        
        if(radioConn == null){
            throw new HeliosException("BEACON RETRNSMIT THREAD : The radioconnection passed to RetransmitThread is null");
        }
        if(dataToSend == null){
            throw new HeliosException("BEACON RETRNSMIT THREAD : The datagram passed to RetransmitThread is null");
        }
        
        this.radioConnection = radioConn;
        this.dataToSend = dataToSend;
        
    }
    
    @Override
    public void run() {
        try {
            BeaconRequestMessage beaconRequestMessage = new BeaconRequestMessage(this.beaconSeqNumber);
            dataToSend.reset();
            this.beaconMesssUtils.encodePacket(beaconRequestMessage, dataToSend);
            while (true) {
                try {
                    System.out.println("BEACON RETRNSMIT THREAD :  Transmitting the message : "+beaconRequestMessage); 
                    radioConnection.send(dataToSend);
                    break;
                } catch (NoRouteException e) {
                     /*
                      * Got a "NoRouteException". Try to send it again and
                      * hopefully the robot has moved in the range of a 
                      * forwarder
                      */
                    System.out.println("BEACON RETRANSMIT THREAD : got exception : " + e.getMessage());
                    continue;
                }

            }
            this.beaconSeqNumber++;
        } catch (Exception ex) {
            System.out.println("BEACON RETRANSMIT THREAD : IO Error while retransmittoing : "+ex.getMessage());
        }
        
    }
    
}
