/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.sender;

import com.sun.spot.multihop.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.multihop.radio.NoRouteException;
import edu.helios.common.HeliosConstants;
import edu.helios.exception.HeliosException;
import edu.helios.message.*;
import edu.helios.stats.UIStats;
import edu.helios.utils.BeaconMessageUtils;
import java.awt.HeadlessException;
import java.util.Timer;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;

/**
 * This thread starts the topology discovery. It sends a beacon 
 * request every 5 seconds. All the forwarders which receive this
 * packet, add themselves to the route and re-broadcast it. When the
 * sun spot on the robot receives the beacon request message, it removes
 * the ID's from it and includes them in a beacon response message and 
 * unicasts it back to the host. This thread receives it and extracts
 * the ID's from the response message. These ID's are passed to the UI 
 * through the UIStats object and the topology rendered.
 * 
 */
public class BeaconThread implements Runnable {

    private int portNumber = 0;
    private UIStats firstUIStats = null;
    private UIStats secondUIStats = null;
    private BeaconMessageUtils beaconMessUtils = new BeaconMessageUtils();
    private int latestSequenceNumber = 0;

    public BeaconThread(int portNumber, UIStats firstUIStats, UIStats secondUIStats) throws HeliosException {

        if (firstUIStats == null) {
            throw new HeliosException("The first uiStat object passed to BeaconThread constructor is null");
        }
        if (secondUIStats == null) {
            throw new HeliosException("The second uiStat object passed to BeaconThread constructor is null");
        }
                

        this.portNumber = portNumber;
        this.firstUIStats = firstUIStats;
        this.secondUIStats = secondUIStats;
    }

    public void run() {

        Datagram sendDatagram = null;
        RadiogramConnection radioConnectionBroadcast = null;
        RadiogramConnection radioConnectionReceive = null;
        Datagram receiveDatagram = null;
        BeaconResponseMessage beaconRespMess = null;
        HeliosMessage recvdMessage = null;
        Timer timer = null;
        BeaconRetransmitThread retransmitThread = null;
        try {
            /*
             * Initialize the connections
             */
            //radioConnectionBroadcast = (RadiogramConnection) Connector.open("radiogram://broadcast:" + this.operatingPort);
            radioConnectionBroadcast = (RadiogramConnection) Connector.open("radiogram://broadcast:" + this.portNumber);
            sendDatagram = radioConnectionBroadcast.newDatagram(HeliosConstants.MAX_DATAGRAM_LENGTH);
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            radioConnectionReceive = (RadiogramConnection) Connector.open("radiogram://:" + this.portNumber);
            receiveDatagram = radioConnectionReceive.newDatagram(radioConnectionReceive.getMaximumLength());
            /*
             * Schedule a thread to send the beacon request every 5 seconds.
             */
            timer = new Timer();
            retransmitThread = new BeaconRetransmitThread(radioConnectionBroadcast, sendDatagram);
            timer.schedule(retransmitThread, HeliosConstants.BEACON_RETRANSMIT_TIMEOUT_PERIOD, HeliosConstants.RETRANSMIT_LIMIT);
        } catch (Exception e) {
            System.err.println("BEACON THREAD : Exception in set up : " + e.getMessage());
            e.printStackTrace();
            //System.exit(1);
        }
        try {
            while (true) {
                /*
                 * Wit for a beacon response message
                 */
                System.out.println("BEACON THREAD : Waiting for a response message");
                radioConnectionReceive.receive(receiveDatagram);
                recvdMessage = beaconMessUtils.decodePacket(receiveDatagram);
                if (recvdMessage.getType() != HeliosConstants.BEACON_RESPONSE_TYPE) {
                    /*
                    * Got an invalid message type. DIscard it and do nothing
                    */
                    System.out.println("BEACON THREAD : Received an unexpected message. Doing nothing.. Mess : " + recvdMessage);
                    receiveDatagram.reset();
                    continue;
                }
                beaconRespMess = (BeaconResponseMessage) recvdMessage;
                if(beaconRespMess.getSeqNumber() <= latestSequenceNumber){
                    /*
                     * Got a duplicate message. Discard it and do nothing
                     */
                    System.out.println("BEACON THREAD : Already seen the mess. Doing nothing. Mess : " + recvdMessage);
                    receiveDatagram.reset();
                    continue;
                }
                //beaconRespMess = sendMessageTransaction(radioConnectionBroadcast, radioConnectionReceive, sendDatagram, receiveDatagram, beaconRequestMessage);
                System.out.println("BEACON THREAD : Received the message : " + beaconRespMess);
                latestSequenceNumber = beaconRespMess.getSeqNumber();
                /*
                 * Add the received message to the UIStats to be rendered by
                 * the UI
                 */
                this.firstUIStats.addRecvdBeaconRespone(beaconRespMess);
                this.secondUIStats.addRecvdBeaconRespone(beaconRespMess);
                System.out.println("\n\nThe beacon Resp mess fo far are : \n"+this.firstUIStats.getRecvdBeacons()+"\n\n");
            }
        } catch (Exception ex) {
            System.out.println("BEACON THREAD : Caught exception in beacon THREAD..");
            ex.printStackTrace();;
        }



    }

    private BeaconResponseMessage sendMessageTransaction(RadiogramConnection radioConnectionBroadcast, RadiogramConnection radioConnectionReceive, Datagram sendDatagram, Datagram receiveDatagram, BeaconRequestMessage messToSend) throws HeliosException {

        HeliosMessage recvdMessage = null;
        BeaconResponseMessage beaconRespMess = null;
        boolean sendFlag = true;
        Timer timer = new Timer();
        BeaconRetransmitThread retransmitThread = null;

        if (radioConnectionBroadcast == null) {
            throw new HeliosException("BEACON THREAD : The passed radio connection is null for the method sendMessageTransaction of BEACON THREAD");
        }
        if (messToSend == null) {
            throw new HeadlessException("BEACON THREAD : The passed message to send is null for the method sendMessageTransaction of BEACON THREAD");
        }

        try {

            if (sendFlag) {
                System.out.println("BEACON THREAD : Sending the message : " + messToSend);
                beaconMessUtils.encodePacket(messToSend, sendDatagram);
                while (true) {
                    try {
                        radioConnectionBroadcast.send(sendDatagram);
                        break;
                    } catch (NoRouteException e) {
                        System.out.println("BEACON THREAD : got exception : " + e.getMessage());
                        continue;
                    }

                }

            }

            System.out.println("BEACON THREAD : Waiting for a beacon response....");
            if (retransmitThread == null) {
                retransmitThread = new BeaconRetransmitThread(radioConnectionBroadcast, sendDatagram);
            }
            if (timer == null) {
                timer = new Timer();
                timer.schedule(retransmitThread, HeliosConstants.BEACON_RETRANSMIT_TIMEOUT_PERIOD, HeliosConstants.RETRANSMIT_LIMIT);
            }


            while (true) {
                /*
                 * if(sendFlag){ System.out.println("From "+this.operatingPort+"
                 * : Sending the message : "+messToSend);
                 * messUtils.encodePacket(messToSend, sendDatagram);
                 * radioConnectionBroadcast.send(sendDatagram); }                  *
                 * System.out.println("From "+this.operatingPort+" : Waiting for
                 * the bot to respond...."); if(retransmitThread == null){
                 * retransmitThread = new
                 * RetransmitThread(radioConnectionBroadcast,
                 * sendDatagram,messToSend); } if(timer == null){ timer = new
                 * Timer(); timer.schedule(retransmitThread,
                 * HeliosConstants.RETRANSMIT_TIMEOUT_PERIOD,
                 * HeliosConstants.RETRANSMIT_LIMIT); }
                 *
                 */
                // Read sensor sample received over the radio
                radioConnectionReceive.receive(receiveDatagram);

                recvdMessage = beaconMessUtils.decodePacket(receiveDatagram);
                if (recvdMessage.getType() != HeliosConstants.BEACON_RESPONSE_TYPE) {
                    System.out.println("BEACON THREAD : Received an unexpected message. Doing nothing.. Mess : " + recvdMessage);
                    sendFlag = false;
                    continue;
                }
                beaconRespMess = (BeaconResponseMessage) recvdMessage;
                /*
                 * if(recvdMessage.getSeqNumber() < beaconSeqNumber){
                 * System.out.println("BEACON THREAD : Got a repeated message of
                 * sequence number. Doing nothing. Mess : "+recvdMessage);
                 * sendFlag= false; continue; }
                 *
                 */
                /*
                 * try{ timer.cancel(); timer.purge(); timer = null;
                 * retransmitThread = null; }catch (Exception ex){
                 * System.out.println("BEACON THREAD : Caught exception whiule
                 * cancelling the timer.."); }
                 *
                 */
                sendFlag = true;


                //break;
            }
        } catch (Exception ex) {
            System.out.println("BEACON THREAD : : Caught an exception while waiting for a message in sendMessageTransaction");
            ex.printStackTrace();
            //System.exit(1);
            //continue;
        }

        return beaconRespMess;

    }
}
