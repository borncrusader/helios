/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.sender;

import com.sun.spot.multihop.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.multihop.radio.NoRouteException;
import edu.helios.exception.HeliosException;
import edu.helios.message.HeliosMessage;
import edu.helios.stats.UIStats;
import java.io.IOException;
import java.util.TimerTask;
import javax.microedition.io.Datagram;

/**
 * This is the thread that is invoked during retransmission.
 * It sends the packet that is passed to its constructor.
 * 
 */
public class RetransmitThread extends TimerTask{
    
    RadiogramConnection radioConnection = null;
    
    private UIStats uiStats = null;
    
    Datagram dataToSend = null;
    
    HeliosMessage message = null;
    
    int operatingPort = 0;

    public RetransmitThread(RadiogramConnection radioConnection, Datagram dataToSend, HeliosMessage message, int port, UIStats uiStats) throws HeliosException{
        
        this.operatingPort = port;
        if(radioConnection == null){
            throw new HeliosException("From "+this.operatingPort+" : The radioconnection passed to RetransmitThread is null");
        }
        if(dataToSend == null){
            throw new HeliosException("From "+this.operatingPort+" : The datagram passed to RetransmitThread is null");
        }
        if(message == null){
            throw new HeliosException("From "+this.operatingPort+" : The message passed to RetransmitThread is null");
        }
        if(uiStats == null){
            throw new HeliosException("From "+this.operatingPort+" : The uiStat sent to the constructor of RetransmitThread is null");
        }
        
        this.radioConnection = radioConnection;
        this.dataToSend = dataToSend;
        this.message = message;
        this.uiStats = uiStats;
        
    }
    
    public void setDataToSend(Datagram dataToSend) {
        this.dataToSend = dataToSend;
    }

    public void setRadioConnection(RadiogramConnection radioConnection) {
        this.radioConnection = radioConnection;
    }
    
    
    @Override
    public void run() {
        try {
            System.out.println("From "+this.operatingPort+" : Time out has happened.. Retransmitting the message : "+message); 
            while (true) {
                try {
                    /*
                     * Unicast the data in a loop till the packet is sent.
                     * The send method can send a "NoRouteException" because
                     * the underlying protocol that sends the datagram is the
                     * default wireles mesh throws it. 
                     */
                    radioConnection.send(dataToSend);
                    break;
                } catch (NoRouteException e) {
                    /*
                     * Got a "NoRouteException". Try to send it again and
                     * hopefully the robot has moved in the range of a 
                     * forwarder
                     */
                    System.out.println("got exception : " + e.getMessage());
                    continue;
                }

            }
            this.uiStats.addRetransmittedPacket(dataToSend.getLength());
        } catch (IOException ex) {
            System.out.println("From "+this.operatingPort+" : IO Error while retransmittoing : "+ex.getMessage());
        }
        
    }
    
}
