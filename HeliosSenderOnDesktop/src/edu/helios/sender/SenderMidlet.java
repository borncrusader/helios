/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.sender;



import com.sun.spot.core.peripheral.Spot;
import com.sun.spot.espot_host.ota.OTACommandServer;
import com.sun.spot.ieee_802_15_4_radio.IRadioPolicyManager;
import com.sun.spot.multihop.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.multihop.radio.NoRouteException;
import edu.helios.common.HeliosConstants;
import edu.helios.common.RobotCommand;
import edu.helios.exception.HeliosException;
import edu.helios.message.DownlinkMessage;
import edu.helios.message.HeliosMessage;
import edu.helios.message.UplinkMessage;
import edu.helios.stats.ReceivedRecord;
import edu.helios.stats.UIStats;
import edu.helios.utils.MessageUtils;
import edu.helios.utils.RobotUtils;
import java.awt.HeadlessException;
import java.io.IOException;
import java.text.DateFormat;
import java.util.*;
import javax.microedition.io.*;

/**
 * This class represents the application that runs on the
 * host connected to the base station. It sends commands to
 * the robot and receives the corresponding light value reading.
 * Depending on the reading, the application decides where the
 * robot has to go next.
 * This class also spawns a beacon thread needed to detect the topology.
 * It also spawns threads which display the collected stats in the GUI.
 */

public class SenderMidlet implements Runnable{
    // Broadcast port on which we listen for sensor samples
    
    private UIStats uiStats = null;
    
    private int operatingPort = 0;
    
    private List recvdRecordList = new ArrayList();
    
    private int sequenceNumber = 1;

    private RadiogramConnection radioConnectionBroadcast = null;
    
    private RadiogramConnection radioConnectionReceive = null;
    
    private Datagram sendDatagram = null;
    
    private Datagram receiveDatagram = null;

    private Timer timer = null;

    private RetransmitThread retransmitThread = null;
    
    private MessageUtils messUtils = new MessageUtils();
    
    private RobotUtils rUtils = new RobotUtils();
    
    private String robotAddress = null;
    
    public SenderMidlet(int listeningPort, UIStats uiStats, String robotAddress) throws HeliosException{
        
        this.operatingPort = listeningPort;
        if(uiStats == null){
            throw new HeliosException("From "+this.operatingPort+" : The uiStat sent to the constructor of SenderMidlet is null");
        }
        
        this.uiStats = uiStats;
        this.robotAddress = robotAddress;
    }   
    
    
    public void run() {

        int returnedValue = 0;
        
        /*
        MessageUtils messUtils = new MessageUtils();
        RobotCommand command = null;
        HeliosMessage recvdMessage = null;
        DownlinkMessage downlinkMessage = null;
        UplinkMessage upMess = null;
        RobotUtils rUtils = new RobotUtils();
        //File f = new File();
        */
        try {
            /*
             * Initialize the class variables 
             */
            //radioConnectionBroadcast = (RadiogramConnection) Connector.open("radiogram://broadcast:" + this.operatingPort);
            radioConnectionBroadcast = (RadiogramConnection) Connector.open("radiogram://" + robotAddress +":" + this.operatingPort);
            sendDatagram = radioConnectionBroadcast.newDatagram(HeliosConstants.MAX_DATAGRAM_LENGTH);
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            radioConnectionReceive = (RadiogramConnection) Connector.open("radiogram://:" + this.operatingPort);
            receiveDatagram = radioConnectionReceive.newDatagram(radioConnectionReceive.getMaximumLength());
        } catch (Exception e) {
             System.err.println("From "+this.operatingPort+" : setUp caught " + e.getMessage());
             e.printStackTrace();
             System.exit(1);
        }

        int returnVal = 0;
        
        /*
         * Start the application.
         */
        while (true) {
            try {
                
               /*
                * Scan for light in all directions to determine in which 
                * direction is maximum.
                */
               if((returnVal = this.scanForLight()) == HeliosConstants.LIGHT_ZERO_IN_SCAN){
                 
                   /*
                    * Found the light value zero in all directions after the scan.
                    * Retrace the path to a location where the light value was not 
                    * zero.
                    */
                   this.retracePath();
                   /*
                    * After retracing the path, scan for light to determine the 
                    * direction in which the maximum light is found.
                    */
                   if(this.scanForLight() == HeliosConstants.LIGHT_THRESHOLD_VALUE_REACHED_INDICATOR){
                       /*
                        * The light threshold value is reached. The robot has reached 
                        * close to the light source. Stopping now.
                        */
                       System.out.println("From "+this.operatingPort+" : Threshold reached... Stopping everything");
                       break;
                   }
               }else if(returnVal == HeliosConstants.LIGHT_THRESHOLD_VALUE_REACHED_INDICATOR){
                  /*
                   * The light threshold value is reached. The robot has reached 
                   * close to the light source. Stopping now.
                   */
                   System.out.println("From "+this.operatingPort+" : Threshold reached... Stopping everything");
                   break;
               }
               
               /*
                * The scan routine has conclusively found a direction in which
                * the light is maximum. Moving ahead in that direction.
                */
               while((returnedValue = this.moveStraight(HeliosConstants.DISTANCE_PER_STRAIGHT_MOVEMENT)) == HeliosConstants.LIGHT_VALUE_INCREASING_INDICATOR);
               /*
                * The returnedValue is something other than "LIGHT_VALUE_INCREASING_INDICATOR". This means
                * that either light value threshold has reached or the light value has decreased for three
                * consecutive readings.
                */
               if(returnedValue == HeliosConstants.LIGHT_THRESHOLD_VALUE_REACHED_INDICATOR){
                   /*
                   * The light threshold value is reached. The robot has reached 
                   * close to the light source. Stopping now.
                   */
                   System.out.println("From "+this.operatingPort+" : Threshold reached... Stopping everything");
                   break;
               }
               
               /*
                * The light value has decreased for three consecutive readings. The
                * robot is now moving away from the light source. Start a scan routine
                * to determine the direction where the robot should turn next.
                */
               
            } catch (Exception e) {
                System.err.println("From "+this.operatingPort+" : Caught " + e +  " while receiving the sensor samples.");
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
    
    /*
     * This method sends the message in the passed datagram and waits for a response
     * message from the robot. It also starts a retransmit thread to handle time outs.
     */
    private UplinkMessage sendMessageTransaction(RadiogramConnection radioConnectionBroadcast, RadiogramConnection radioConnectionReceive, Datagram sendDatagram, Datagram receiveDatagram,DownlinkMessage messToSend) throws HeliosException{
        
        HeliosMessage recvdMessage = null;
        UplinkMessage upMess = null;
        boolean sendFlag = true;
        
        if(radioConnectionBroadcast == null){
            throw new HeadlessException("From "+this.operatingPort+" : The passed radio connection is null for the method sendMessageTransaction");
        }
        if(messToSend == null){
            throw new HeadlessException("From "+this.operatingPort+" : The passed message to send is null for the method sendMessageTransaction");
        }
        
        try{
        
            if(sendFlag){
                System.out.println("From "+this.operatingPort+" : Sending the message : "+messToSend);
                /*
                 * Encode the packet to be sent
                 */
                messUtils.encodePacket(messToSend, sendDatagram);
                while (true) {
                    /*
                     * Unicast the data in a loop till the packet is sent.
                     * The send method can send a "NoRouteException" because
                     * the underlying protocol that sends the datagram is the
                     * default wireles mesh throws it. 
                     */
                    try {
                        radioConnectionBroadcast.send(sendDatagram);
                        break;
                    } catch (NoRouteException e) {
                        System.out.println("got exception : " + e.getMessage());
                        /*
                         * Got a "NoRouteException". Try to send it again and
                         * hopefully the robot has moved in the range of a 
                         * forwarder
                         */
                        continue;
                    }

                }
                
                /*
                 * Add the sent message info to the UIStats which will be displayed 
                 * by the UI.
                 */
                this.uiStats.addSentPacket(messToSend, sendDatagram.getLength());
            }    
                
            System.out.println("From "+this.operatingPort+" : Waiting for the bot to respond....");
            /*
             * Start a retransmit thread for the sent packet.
             */
            if(retransmitThread == null){
                retransmitThread = new RetransmitThread(radioConnectionBroadcast, sendDatagram,messToSend,this.operatingPort, this.uiStats);
            }
            if(timer == null){
                timer = new Timer();
                timer.schedule(retransmitThread, HeliosConstants.RETRANSMIT_TIMEOUT_PERIOD, HeliosConstants.RETRANSMIT_LIMIT);
            }
        
        
            while(true){
                    
                    /*
                     * Wait for the robot to respond
                     */
                    radioConnectionReceive.receive(receiveDatagram);
        
                    /*
                     * Received a message. Decode it.
                     */
                    recvdMessage = messUtils.decodePacket(receiveDatagram);
                    if(recvdMessage.getType() != HeliosConstants.UPLINK_MESS_TYPE){
                        /*
                         * Received a message of an unexpected type. Discard and do nothing
                         */
                        System.out.println("From "+this.operatingPort+" : Received an unexpected message. Doing nothing.. Mess : "+recvdMessage);
                        sendFlag = false;
                        continue;
                    }
                    upMess = (UplinkMessage) recvdMessage;
                    if(recvdMessage.getSeqNumber() < sequenceNumber){
                        /*
                         * Got a repeated message. Ignoring it.
                         */
                        System.out.println("From "+this.operatingPort+" : Got a repeated message of sequence number. Doing nothing. Mess : "+recvdMessage);
                        sendFlag= false;
                        continue;
                    }
                    /*
                     * Got the response message for the sent request message.
                     * Cancel the retransmit thread started for this packet.
                     */
                    try{
                        timer.cancel();
                        timer.purge();
                        timer = null;
                        retransmitThread = null;
                    }catch (Exception ex){
                        System.out.println("From "+this.operatingPort+" : Caught exception whiule cancelling the timer.."); 
                    }
                    sendFlag = true;
            
                    /*
                     * Return to main function
                     */
                    break;
                }
            }catch (Exception ex){
                System.out.println("From "+this.operatingPort+" : Caught an exception while waiting for a message in sendMessageTransaction");
                ex.printStackTrace();
                System.exit(1);
                //continue;
            }
        
        return upMess;
        
    }
    
    /*
     * This method sends a command to the robot instructing it to
     * move forward for the distance passed (10 inches). At the 
     * end of this, it receives the light value and determines
     * if the light value is decreasing for the last three values.
     * If it is, it returns "LIGHT_VALUE_DECREASING_INDICATOR" to
     * tell that the robot has strayed ofcourse and a scan mode 
     * should now start.
     * If the light value returned is greater tahn the threshold
     * value, it returns "LIGHT_THRESHOLD_VALUE_REACHED_INDICATOR"
     * to indicate that the robot has closed close to the light and
     * it should stop now.
     */
    private int moveStraight(int distance) throws HeliosException{
        
        RobotCommand command = null;
        DownlinkMessage downlinkMessage = null;
        UplinkMessage upMess = null;
        boolean violation = false;
        
        /*
         * Construct the command to move straight
         */
        command = rUtils.constructCommand(HeliosConstants.MOVE_FORWARD_CMD, distance);
        downlinkMessage = new DownlinkMessage(this.sequenceNumber, command);
        /*
         * Send the command
         */
        upMess = sendMessageTransaction(radioConnectionBroadcast, radioConnectionReceive, sendDatagram, receiveDatagram, downlinkMessage);
        System.out.println("From "+this.operatingPort+" : Received the message : "+upMess);
        if(upMess.getValue() >= HeliosConstants.LIGHT_SOURCE_SENSED_THRESHOLD){
            return HeliosConstants.LIGHT_THRESHOLD_VALUE_REACHED_INDICATOR;
        }
        addToReceivedRecord(upMess, command);
        /*
         * Add the received message to the UIStats to be displayed
         * in the UI
         */
        this.uiStats.addReceivedPacket(new ReceivedRecord(upMess.getSeqNumber(), command, upMess), receiveDatagram.getLength());
        sequenceNumber = upMess.getSeqNumber();
        sequenceNumber++;
        receiveDatagram.reset();
        sendDatagram.reset();
        /*
         * Check if the light value is decreasing for the last three readings.
         */
        violation = checkProgress();
        if(violation){
            return HeliosConstants.LIGHT_VALUE_DECREASING_INDICATOR;
        }else{
            return HeliosConstants.LIGHT_VALUE_INCREASING_INDICATOR;
        }
         
      
    }
    
    /*
     * This method checks the progress of the robot. It returns true
     * if the light value is not decreasing for the last 3 readings
     * indicating that the robot has strayed ofcourse. It returns 
     * true if the light value is not decreasing for three consecutive 
     * readings denoting that the robot is on course.
     */
    private boolean checkProgress(){
        
        int lightReading = 0;
        int prevLightReading = 0;
        int index = 0;
        int recordsLenght = this.recvdRecordList.size();
        boolean violation = false;
  
        if(recordsLenght > HeliosConstants.VIOLATION_CHECK_THRESHOLD_RECORDS){
            index = recordsLenght - HeliosConstants.VIOLATION_CHECK_THRESHOLD_RECORDS;
            prevLightReading = ((ReceivedRecord)this.recvdRecordList.get(index - 1)).getReceivedMessage().getValue();
            while(index < recordsLenght){
                lightReading = ((ReceivedRecord)this.recvdRecordList.get(index)).getReceivedMessage().getValue();
                if(prevLightReading >= lightReading){
                    violation = true;
                }else{
                    violation = false;
                    break;
                }
                prevLightReading = lightReading;
                index++;
            }
        }
        
        return violation;   
        
    }
    
    /*
     * This method retraces the robot back to the point
     * where the last non zero light reading was seen. This
     * method is called when the scan returns non zero value 
     * for all directions.
     */
    private void retracePath() throws HeliosException{
        
        RobotCommand command = null;
        DownlinkMessage downlinkMessage = null;
        UplinkMessage upMess = null;
        RobotCommand prevCommand = null;
        List copyRecvdRecord = new ArrayList();
        copyRecvdRecord.addAll(this.recvdRecordList);
        int recordSize = copyRecvdRecord.size();
        int index = recordSize - 1;
        int totalAngle = 360;
        int numberOfTurns = totalAngle/HeliosConstants.ANGLE_COVERED_PER_TURN;
        index -= numberOfTurns; 
        
        System.out.println("\n\nFrom "+this.operatingPort+" : *****************************************RETRACING");
        
        while(index > 0){
            if(((ReceivedRecord)copyRecvdRecord.get(index)).getReceivedMessage().getValue() == 0){
                prevCommand = ((ReceivedRecord)copyRecvdRecord.get(index)).getCommand();
                if(prevCommand.getCommandType() == HeliosConstants.MOVE_FORWARD_CMD){
                    command = rUtils.constructCommand(HeliosConstants.MOVE_REVERSE_CMD, HeliosConstants.DISTANCE_PER_STRAIGHT_MOVEMENT);
                }else if(prevCommand.getCommandType() == HeliosConstants.MOVE_REVERSE_CMD){
                    command = rUtils.constructCommand(HeliosConstants.MOVE_FORWARD_CMD, HeliosConstants.DISTANCE_PER_STRAIGHT_MOVEMENT);
                }else{
                    command = null;
                }
                if(command != null){
                    downlinkMessage = new DownlinkMessage(this.sequenceNumber, command);
                    upMess = sendMessageTransaction(radioConnectionBroadcast, radioConnectionReceive, sendDatagram, receiveDatagram, downlinkMessage);
                    System.out.println("From "+this.operatingPort+" : Received the message : "+upMess);
                    addToReceivedRecord(upMess, command); 
                    this.uiStats.addReceivedPacket(new ReceivedRecord(upMess.getSeqNumber(), command, upMess), receiveDatagram.getLength());
                    sequenceNumber = upMess.getSeqNumber();
                    sequenceNumber++;
                    receiveDatagram.reset();
                    sendDatagram.reset();                  
                }
                index--;
            }else{
                System.out.println("From "+this.operatingPort+" : Found a non zero value. Stopping the tracing..");
                break;
            }
            
        }
        
        System.out.println("From "+this.operatingPort+" : **************************************DONE RETRACING\n\n");
        
    }
    
    /*
     * This method scans for light in all possible directions. It instructs 
     * the robot turns 45 degrees every step and return the light value recorded.
     * This is repeated 8 times to cover 360 degrees.
     * 
     * If the light value is zero in all possible directions, it returns
     * "LIGHT_ZERO_IN_SCAN" so that retrace can be called.
     * 
     * If the light value returned is greater than the threshold
     * value, it returns "LIGHT_THRESHOLD_VALUE_REACHED_INDICATOR"
     * to indicate that the robot has closed close to the light and
     * it should stop now.
     */
    private int scanForLight() throws HeliosException{

        RobotCommand command = null;
        DownlinkMessage downlinkMessage = null;
        UplinkMessage upMess = null;
        int totalAngle = 360;
        int highestReadingIndex = 0;
        int highestReadingAngle = 0;
        int highestValue = 0;
        int numberOfTurns = totalAngle/HeliosConstants.ANGLE_COVERED_PER_TURN;
        //int currentAngle = HeliosConstants.ANGLE_COVERED_PER_TURN;
        int index = 0;
        boolean nonZeroValue = false;
        List receivedReadings = new ArrayList();
        
        System.out.println("\n\nFrom "+this.operatingPort+" : ##############Starting the scan. Will scan at angle of "+HeliosConstants.ANGLE_COVERED_PER_TURN+" for "+numberOfTurns+" times...");
        
        index = numberOfTurns;
        while(index > 0){
            /*
             * Construct the command to turn 45 degrees to the left
             */
            command = rUtils.constructCommand(HeliosConstants.MOVE_LEFT_CMD, HeliosConstants.ANGLE_COVERED_PER_TURN);
            downlinkMessage = new DownlinkMessage(this.sequenceNumber, command);
            upMess = sendMessageTransaction(radioConnectionBroadcast, radioConnectionReceive, sendDatagram, receiveDatagram, downlinkMessage);
            System.out.println("From "+this.operatingPort+" : Received the message : "+upMess);
            /*
             * Add the received message to the previous records received.
             */
            addToReceivedRecord(upMess, command);
            if(upMess.getValue() > 0){
                nonZeroValue = true;
            }
            if(upMess.getValue() > HeliosConstants.LIGHT_SOURCE_SENSED_THRESHOLD){
                System.out.println("From "+this.operatingPort+" : Threshold reached... Stopping everything");
                return HeliosConstants.LIGHT_THRESHOLD_VALUE_REACHED_INDICATOR;
            }
            receivedReadings.add(new ReceivedRecord(upMess.getSeqNumber(), command, upMess));
            /*
             * Add the received message to the UIStat to be displayed in
             * the UIs
             */
            this.uiStats.addReceivedPacket(new ReceivedRecord(upMess.getSeqNumber(), command, upMess), receiveDatagram.getLength());
            sequenceNumber = upMess.getSeqNumber();
            sequenceNumber++;
            receiveDatagram.reset();
            sendDatagram.reset();
            index--;
        }
        
        if(nonZeroValue){
            index = 0;
            while(index < numberOfTurns){
                if(highestValue < ((ReceivedRecord)receivedReadings.get(index)).getReceivedMessage().getValue()){
                    highestValue = ((ReceivedRecord)receivedReadings.get(index)).getReceivedMessage().getValue();
                    highestReadingIndex = index;
                }
                index++;
            }
        
            highestReadingAngle = (highestReadingIndex + 1) * HeliosConstants.ANGLE_COVERED_PER_TURN;
            System.out.println("From "+this.operatingPort+" : Found the highest value at angle "+highestReadingAngle);
            System.out.println("From "+this.operatingPort+" : Turning to the highest reading angle....");
            command = rUtils.constructCommand(HeliosConstants.MOVE_LEFT_CMD, highestReadingAngle);
            downlinkMessage = new DownlinkMessage(this.sequenceNumber, command);
            upMess = sendMessageTransaction(radioConnectionBroadcast, radioConnectionReceive, sendDatagram, receiveDatagram, downlinkMessage);
            System.out.println("From "+this.operatingPort+" : ##############SCANNING DONE Received the message : "+upMess+"\n\n");
            addToReceivedRecord(upMess, command);
            this.uiStats.addReceivedPacket(new ReceivedRecord(upMess.getSeqNumber(), command, upMess), receiveDatagram.getLength());

            sequenceNumber = upMess.getSeqNumber();
            sequenceNumber++;
            receiveDatagram.reset();
            sendDatagram.reset();
            receivedReadings.clear();
        }else{
            //this.retracePath();
        }
        
        if(!nonZeroValue){
            /*
             * Light is zero in all directions
             */
            return HeliosConstants.LIGHT_ZERO_IN_SCAN;
        }else{
            /*
             * Light is non zero in at least one direction.
             */
            return HeliosConstants.LIGHT_NON_ZERO_IN_SCAN;
        }
        
    }
    
    /*
     * This method adds the received message to the received record 
     * to be used to make decisions on what the robot should do next
     */
    private void addToReceivedRecord(UplinkMessage upMess, RobotCommand command) throws HeliosException{
        
        this.recvdRecordList.add(new ReceivedRecord(upMess.getSeqNumber(), command, upMess));
        if(this.recvdRecordList.size() > HeliosConstants.RECEIVE_RECORD_COUNT_LIMIT){
            //System.out.println("Removng the entry : "+((ReceivedRecord)this.recvdRecordList.get(0)).getReceivedMessage());
            this.recvdRecordList.remove(0);
        }
    }
    
    
    /**
     * Start up the host application.
     *
     * @param args any command line arguments
     */
    public static void main(String[] args) throws Exception {
        // register the application's name with the OTA Command server & start OTA running
        //OTACommandServer.start("SendDataDemo");
        
        /*
         * The host application keeps track of two robots by spawning two seperate threads.
         * Each thread caters to one robot. The ports used for communication is different for
         * both robots to ensure that they are isolated.
         * 
         * The host application passes the same UIStat object to the SenderThread and the 
         * UIThread for each robot. The sender thread writes the collected stats in the shared 
         * object toi be displayed by the UIThread.
         * 
         * The host application also spawns the beacon thread to keep track of the topology of
         * the network.
         */

        /*
         * Construct the UIStat object for the first robot
         */
        UIStats firstRobotStats = new UIStats();
        /*
         * Spawn the monitor window thread to display the stats in the UI for the 
         * first robot
         */
        MonitorWindow firstRobotWindow = new MonitorWindow("Helios Monitor - Robot 1", firstRobotStats, HeliosConstants.ROBOT1_ADDRESS);
        Thread guiThread1 = new Thread(firstRobotWindow);
        guiThread1.start();
        /*
         * Spawn the thread to guide the fisrt robot to the light.
         */
        SenderMidlet firstRobotControler = new SenderMidlet(HeliosConstants.ROBOT_PORT1, firstRobotStats, HeliosConstants.ROBOT1_ADDRESS);
        Thread thread1 = new Thread(firstRobotControler);
        thread1.start();
        
        
        /*
         * Construct the UIStat object for the second robot
         */
        UIStats secondRobotStats = new UIStats();
        /*
         * Spawn the monitor window thread to display the stats in the UI for the 
         * second robot
         */
        MonitorWindow secondRobotWindow = new MonitorWindow("Helios Monitor - Robot 2", secondRobotStats, HeliosConstants.ROBOT2_ADDRESS);
        Thread guiThread2 = new Thread(secondRobotWindow);
        guiThread2.start();
        /*
         * Spawn the thread to guide the second robot to the light.
         */
        SenderMidlet secondRobotControler = new SenderMidlet(HeliosConstants.ROBOT_PORT2, secondRobotStats, HeliosConstants.ROBOT2_ADDRESS);
        Thread thread2 = new Thread(secondRobotControler);
        thread2.start();
       
        /*
         * Spawn the beacon thread to keep track of topology.
         */
        Thread beaconThread = new Thread(new BeaconThread(HeliosConstants.BEACON_PORT_NUMBER, firstRobotStats, secondRobotStats));
        beaconThread.start();
    }
}

