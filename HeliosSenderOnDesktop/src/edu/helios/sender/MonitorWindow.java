/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.sender;

import edu.helios.exception.HeliosException;
import edu.helios.common.HeliosConstants;
import edu.helios.common.RobotCommand;
import edu.helios.message.BeaconResponseMessage;
import edu.helios.stats.ReceivedRecord;
import edu.helios.stats.UIStats;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/* path plotting module */
class PathPlot extends JComponent {
    private int lastSeqNum = 0;
    private int angle = 0;
    private int cur_x = 100;
    private int cur_y = 100;
    
    private List records = null;
    
    public void PathPlot() {
    }
    
    public void setRecvdRecords(List records) {
        this.records = records;
        this.repaint();
    }
/* every time we paint, we'll call this.. to update everything properly..
 * 
 */
    public void paint(Graphics g) {
        int next_x = 0, next_y = 0;
        int cur_x = 300, cur_y = 200;
        int angle = 0;
        int i;
        RobotCommand cmd;

        /* interesting trigonometry magic! :)
         * 
         */
        for (i = 0; i < records.size(); i++) {
            cmd = ((ReceivedRecord) records.get(i)).getCommand();
            if (cmd.getCommandType() == HeliosConstants.MOVE_LEFT_CMD) {
                angle -= cmd.getDuration() * 90 / 500;
            } else if (cmd.getCommandType() == HeliosConstants.MOVE_RIGHT_CMD) {
                angle += cmd.getDuration() * 90 / 500;
            } else if (cmd.getCommandType() == HeliosConstants.MOVE_FORWARD_CMD) {
                next_x = (int) (cur_x + (cmd.getDuration() * 30 / 1600) * Math.cos(Math.toRadians(angle)));
                next_y = (int) (cur_y + (cmd.getDuration() * 30 / 1600) * Math.sin(Math.toRadians(angle)));
                g.drawLine(cur_x, cur_y, next_x, next_y);
                cur_x = next_x;
                cur_y = next_y;
            }
        }
    }
    
    // not used!
    public void addLink(int seqNum, int cmd, int arg) {
        Graphics g = getGraphics();
        int next_x = 0, next_y = 0;
        
        if (seqNum == lastSeqNum) {
            return;
        }
        
        lastSeqNum = seqNum;
        
        System.out.println("ADDING LINK! " + cmd + " " + arg);
        if (cmd == HeliosConstants.MOVE_LEFT_CMD) {
            angle -= arg*90/500;
        } else if (cmd == HeliosConstants.MOVE_RIGHT_CMD) {
            angle += arg*90/500;
        } else if (cmd == HeliosConstants.MOVE_FORWARD_CMD) {
            next_x = (int) (cur_x + (arg*30/1600) * Math.cos(Math.toRadians(angle)));
            next_y = (int) (cur_y + (arg*30/1600) * Math.sin(Math.toRadians(angle)));
            g.drawLine(cur_x, cur_y, next_x, next_y);
            cur_x = next_x;
            cur_y = next_y;
        }
    }
}
/* topology creation module */
class TopologyPlot extends JComponent {
    Vector ids;
    String robot = null;
    String bs = null;
    
    int lastSeqNum = 0;
    int linger = 0;
    
    BeaconResponseMessage rsp = null;
    
    public TopologyPlot(Vector s, String robot) {
        ids = s;
        this.robot = robot.substring(robot.length()-4);
        this.bs = HeliosConstants.BASE_STATION_ADDRESS.substring(robot.length()-4);
    }
    
    /*
    public void setTopologyIds(Vector ids, int lastSeqNum) {
        this.ids = ids;
        if (lastSeqNum == this.lastSeqNum) {
            linger++;
        } else {
            linger = 0;
        }
        this.repaint();
    }
    *
    */
    public void setTopologyIds(UIStats stats) {
        if (stats.getLatestRecvdBeacon() == this.lastSeqNum) {
            linger++;
        } else {
            linger = 0;
        }
        this.lastSeqNum = stats.getLatestRecvdBeacon();
        
        if (stats.getRecvdBeacons().size() > 0) {
            int size = stats.getRecvdBeacons().size();
            this.rsp = (BeaconResponseMessage) stats.getRecvdBeacons().get(size - 1);
        } else {
            this.rsp = null;
        }
        
        this.repaint();
    }
    /* paint the graphics widget from the current received beacon */
    public void paint(Graphics g) {
        Image img = null;
        int i = 0, j;
        int bsOffset = 0;
        
        // robot
        img = new ImageIcon("wolf.jpg").getImage();
        g.drawImage(img, i * 50 + 35, 35, 30, 30, this);
        g.drawArc(i*50,0, 100, 100, 0, 360);
        g.drawString(robot+"", i*50+30, 120);
        i++;

        // intermediate  nodes\
        if (rsp != null) {
            Vector route = rsp.getRoute();

            img = new ImageIcon("java.png").getImage();            
            for (j=0; j<rsp.getLength(); j++, i++) {
                Integer id = (Integer) route.elementAt(j);
                g.drawArc(i * 50, 0, 100, 100, 0, 360);
                g.drawImage(img, i * 50 + 35, 35, 30, 30, this);
                g.drawString(Integer.toHexString(id.intValue()), i * 50 + 30, 120);
            }
        }
        if (linger > HeliosConstants.TOPOLOGY_TIMEOUT_THRESHOLD) {
            bsOffset = 50;
        }
        // basestation
        img = new ImageIcon("station.jpg").getImage();
        g.drawArc(i*50+bsOffset,0, 100, 100, 0, 360);
        g.drawImage(img, i*50+35+bsOffset, 35, 30, 30, this);
        g.drawString(bs+"", i*50+30+bsOffset, 120);
    }
}

public class MonitorWindow extends JFrame implements Runnable{
    private javax.swing.JTabbedPane heliosTabPane;
    private javax.swing.JPanel statsTabPane;
    private javax.swing.JPanel plotTabPane;
    private javax.swing.JPanel pathTabPane;
    private javax.swing.JPanel topoTabPane;
    private TopologyPlot topoPlot;
    private PathPlot pathPlot;
    
    private ChartPanel plotChartPanel;
    private JFreeChart plotChart;
    private javax.swing.JScrollPane statsScrollPane;
    private javax.swing.JTextPane statsTextArea;
    
    private UIStats stats;
    private String robot;
    
    public MonitorWindow(String s, UIStats stats, String robot) throws HeliosException {
        if (s == null) {
            throw new HeliosException("title not used for MonitorWindow");
        }
        if (stats == null) {
            throw new HeliosException("stats null for MonitorWindow");
        }
        this.stats = stats;
        this.robot = robot;
        setTitle(s);
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    private void initComponents() {
        createTabMenu();
        pack();
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }
    // update the stats tab
    public void updateStatistics() {
        statsTextArea.setText(stats.toString());
    }
    // update the plot for the first time.. afterwards, we'll call updatePlot
    private void createPlot() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        String ptitle = "Packet Statistics";
        String xaxis = "";
        String yaxis = "Number of packets";
        PlotOrientation orientation = PlotOrientation.HORIZONTAL;
        
        plotChart = ChartFactory.createBarChart(ptitle, xaxis, yaxis, dataset, orientation, true, true, true);
        plotChartPanel = new ChartPanel(plotChart);
        plotTabPane.add(plotChartPanel, BorderLayout.CENTER);
    }
    // update the plot from the stats
    public void updatePlot() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        dataset.addValue(stats.getNumberOfPacketsSent(), "Packets Sent", "Packet Statistics");
        dataset.addValue(stats.getNumberOfPacketsReceived(), "Packets Received", "Packet Statistics");
        dataset.addValue(stats.getNumberOfPacketsRetransmitted(), "Packets Retransmitted", "Packet Statistics");
        dataset.addValue(stats.getNumberOfBytesSent(), "Bytes Sent", "Bytes Statistics");
        dataset.addValue(stats.getNumberOfBytesReceived(), "Bytes Received", "Bytes Statistics");
        dataset.addValue(stats.getNumberOfBytesRetransmitted(), "Bytes Retransmitted", "Bytes Statistics");
        
        String ptitle = "";
        String xaxis = "";
        String yaxis = "";
        PlotOrientation orientation = PlotOrientation.HORIZONTAL;
        
        plotChart = ChartFactory.createBarChart(ptitle, xaxis, yaxis, dataset, orientation, true, true, true);
        plotChartPanel.setChart(plotChart);
    }
    // update the topology from the received records
    public void updateTopology() {
        /*
        if (stats.getLatestRecvdMessage() == null ||
                stats.getLatestRecvdMessage().getReceivedMessage() == null) {
            return;
        }
        Vector v = stats.getLatestRecvdMessage().getReceivedMessage().getRoute();
        int lastSeqNum = stats.getLatestSequenceNumberReceived();
        topoPlot.setTopologyIds(v, lastSeqNum);
        * 
        */
        
       
        
        topoPlot.setTopologyIds(stats);
    }
    // update the path from the last movements
    public void updatePathPlot() {
        /*
        if (stats.getLatestRecvdMessage() == null) {
            return;
        }
        int seqNum = stats.getLatestRecvdMessage().getSequenceNumber();
        RobotCommand command = stats.getLatestRecvdMessage().getCommand();
        
        pathPlot.addLink(seqNum, command.getCommandType(), command.getDuration());
        *
        */
        pathPlot.setRecvdRecords(stats.getRecvdRecords());
        return;
    }
    
    // called when constructor is called - create the layouts
    private void createTabMenu() {
        //Create the tab pane
        heliosTabPane = new JTabbedPane();
        
        //Build Statistics tab
        statsTabPane = new JPanel(new BorderLayout());
        statsTabPane.setOpaque(true);
        //Create text area
        statsTextArea = new JTextPane();
        statsTextArea.setEditable(false);
        //Add scrolled text area 
        statsScrollPane = new JScrollPane(statsTextArea);        
        //Add text area to stats pane
        statsTabPane.add(statsScrollPane);        
        heliosTabPane.add("Statistics",statsTabPane);
        //updateStatistics();
      
        //Build Plot tab
        plotTabPane = new JPanel(new BorderLayout());
        plotTabPane.setOpaque(true);
        createPlot();
        heliosTabPane.add("Plot",plotTabPane);
        
        //Build Path tab
        pathTabPane = new JPanel(new BorderLayout());
        pathTabPane.setOpaque(true);
        pathPlot = new PathPlot();
        pathTabPane.add(pathPlot);
        heliosTabPane.add("Path",pathTabPane);
        
        //Build Topology tab
        topoTabPane = new JPanel(new BorderLayout());
        topoTabPane.setOpaque(true);
        topoPlot = new TopologyPlot(null, robot);
        topoTabPane.add(topoPlot);
        heliosTabPane.add("Topology",topoTabPane);
        
        this.add(heliosTabPane, BorderLayout.CENTER);
    }
/* entry point for this thread - will just set the window as visible
 * and every 500 ms, call update functions to update GUI
 */
    public void run() {
        int num = 0;
        this.setSize(600, 400);
        this.validate();
        this.setVisible(true);

        while (true) {
            num++;
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
            }
            updateStatistics();
            updatePlot();
            updatePathPlot();
            //if (num%10 == 0) {
                updateTopology();
            //}
        }
    }
}
