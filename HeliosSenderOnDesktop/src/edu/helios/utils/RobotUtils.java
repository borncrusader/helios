/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.utils;

import edu.helios.exception.HeliosException;
import edu.helios.common.HeliosConstants;
import edu.helios.common.RobotCommand;

/*
 * This class is an utility class that is used to
 * construct robot commands to be sent by the base 
 * station 
 */
public class RobotUtils {
   
    /* commandArgument will be in distance (either cm or deg) or 
     * in degrees (angle). This function translates between the 
     * passed argument type and time (how long the robot should 
     * execute the command to cover the distance or angle).
     */
    public RobotCommand constructCommand(int commandType, int commandArgument) throws HeliosException {
        
        RobotCommand command = null;
        
        try {
            if (commandType == HeliosConstants.MOVE_FORWARD_CMD ||
                    commandType == HeliosConstants.MOVE_REVERSE_CMD) {
                if (commandArgument < 1) {
                    throw new HeliosException("distance is less than 1 cm!");
                }
                command = new RobotCommand(commandType, commandArgument*1600/30);
                
            } else if (commandType == HeliosConstants.MOVE_LEFT_CMD ||
                    commandType == HeliosConstants.MOVE_RIGHT_CMD) {
                commandArgument %= 361;
                if (commandArgument < 5) {
                    throw new HeliosException("turn angle is less than 5 degrees!");
                }
                command = new RobotCommand(commandType, commandArgument*500/90);
            }
        } catch (HeliosException e) {
            throw e;
        }
        
        return command;
    }
    
}
