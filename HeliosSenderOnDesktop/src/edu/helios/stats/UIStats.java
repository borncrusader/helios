/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.stats;

import edu.helios.exception.HeliosException;
import edu.helios.message.BeaconResponseMessage;
import edu.helios.message.DownlinkMessage;
import edu.helios.message.UplinkMessage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Shreyas
 */
public class UIStats {
    
    private List recvdBeacons = new ArrayList();
    
    private int latestRecvdBeacon = 0;
    
    private List recvdRecords = new ArrayList();
    
    private int latestSequenceNumberSent = 0;
    
    private int latestSequenceNumberReceived = 0;
    
    private List sentRecords = new ArrayList();
    
    private int numberOfPacketsSent = 0;
    
    private int numberOfPacketsReceived = 0;
    
    private int numberOfPacketsRetransmitted = 0;
    
    private int numberOfBytesSent = 0;
    
    private int numberOfBytesReceived = 0;
    
    private int numberOfBytesRetransmitted = 0;
    
    private long startTransmissionTime = 0;
    
    private long latestReceivedMessagTime = 0;

    public int getLatestSequenceNumberReceived() {
        return latestSequenceNumberReceived;
    }

    public void setLatestSequenceNumberReceived(int latestSequenceNumberReceived) {
        this.latestSequenceNumberReceived = latestSequenceNumberReceived;
    }

    public int getLatestSequenceNumberSent() {
        return latestSequenceNumberSent;
    }

    public void setLatestSequenceNumberSent(int latestSequenceNumberSent) {
        this.latestSequenceNumberSent = latestSequenceNumberSent;
    }

    public int getNumberOfBytesReceived() {
        return numberOfBytesReceived;
    }

    public void setNumberOfBytesReceived(int numberOfBytesReceived) {
        this.numberOfBytesReceived = numberOfBytesReceived;
    }

    public int getNumberOfBytesRetransmitted() {
        return numberOfBytesRetransmitted;
    }

    public void setNumberOfBytesRetransmitted(int numberOfBytesRetransmitted) {
        this.numberOfBytesRetransmitted = numberOfBytesRetransmitted;
    }

    public int getNumberOfBytesSent() {
        return numberOfBytesSent;
    }

    public void setNumberOfBytesSent(int numberOfBytesSent) {
        this.numberOfBytesSent = numberOfBytesSent;
    }

    public int getNumberOfPacketsReceived() {
        return numberOfPacketsReceived;
    }

    public void setNumberOfPacketsReceived(int numberOfPacketsReceived) {
        this.numberOfPacketsReceived = numberOfPacketsReceived;
    }

    public int getNumberOfPacketsRetransmitted() {
        return numberOfPacketsRetransmitted;
    }

    public void setNumberOfPacketsRetransmitted(int numberOfPacketsRetransmitted) {
        this.numberOfPacketsRetransmitted = numberOfPacketsRetransmitted;
    }

    public int getNumberOfPacketsSent() {
        return numberOfPacketsSent;
    }

    public void setNumberOfPacketsSent(int numberOfPacketsSent) {
        this.numberOfPacketsSent = numberOfPacketsSent;
    }

    public List getRecvdRecords() {
        return recvdRecords;
    }

    public void setRecvdRecords(List recvdRecords) {
        this.recvdRecords = recvdRecords;
    }

    public List getSentRecords() {
        return sentRecords;
    }

    public void setSentRecords(List sentRecords) {
        this.sentRecords = sentRecords;
    }

    public long getLatestReceivedMessagTime() {
        return latestReceivedMessagTime;
    }

    public void setLatestReceivedMessagTime(long latestReceivedMessagTime) {
        this.latestReceivedMessagTime = latestReceivedMessagTime;
    }

    public long getStartTransmissionTime() {
        return startTransmissionTime;
    }

    public void setStartTransmissionTime(long startTransmissionTime) {
        this.startTransmissionTime = startTransmissionTime;
    }

    public List getRecvdBeacons() {
        return recvdBeacons;
    }

    public void setRecvdBeacons(List recvdBeacons) {
        this.recvdBeacons = recvdBeacons;
    }

    public int getLatestRecvdBeacon() {
        return latestRecvdBeacon;
    }

    public void setLatestRecvdBeacon(int latestRecvdBeacon) {
        this.latestRecvdBeacon = latestRecvdBeacon;
    }
    
    
    
    
    public void addSentPacket(DownlinkMessage sentMessage, int bytesSent) throws HeliosException{
        
        if(sentMessage == null){
            throw new HeliosException("The sent message passed to the addSentPacket of UIStats is null");
        }
        if(this.sentRecords.isEmpty()){
            this.startTransmissionTime = System.currentTimeMillis();
        }
        
        this.sentRecords.add(sentMessage);
        if(this.latestSequenceNumberSent < sentMessage.getSeqNumber()){
            this.latestSequenceNumberSent = sentMessage.getSeqNumber();
        }
        this.numberOfBytesSent += bytesSent;
        this.numberOfPacketsSent++;
    }
    
    public void addReceivedPacket(ReceivedRecord recvdRecord, int bytesRecvd) throws HeliosException{
        
        if(recvdRecord == null){
            throw new HeliosException("The recvd record passed to the addSentPacket of UIStats is null");
        }
        
        this.latestReceivedMessagTime = System.currentTimeMillis();
        this.recvdRecords.add(recvdRecord);
        if(this.latestSequenceNumberReceived < recvdRecord.getReceivedMessage().getSeqNumber()){
            this.latestSequenceNumberReceived = recvdRecord.getReceivedMessage().getSeqNumber();
        }
        this.numberOfBytesReceived += bytesRecvd;
        this.numberOfPacketsReceived++;
    }
    
    public DownlinkMessage getLatestSentMessage(){
        
        int size = this.sentRecords.size();
        DownlinkMessage downlinkMess = null;
        if(size > 0){
            downlinkMess = (DownlinkMessage) this.sentRecords.get(size - 1);
        }
        
        return downlinkMess;
    }

    public ReceivedRecord getLatestRecvdMessage(){
        
        int size = this.recvdRecords.size();
        
        ReceivedRecord recvdRecord = null;
        if(size > 0){
            recvdRecord = (ReceivedRecord) this.recvdRecords.get(size - 1);
        }
        
        return recvdRecord;
    }

    public void addRetransmittedPacket(int bytesRetransmitted){
        
        this.numberOfPacketsRetransmitted++;
        this.numberOfBytesRetransmitted += bytesRetransmitted;
    }
    
    public void addRecvdBeaconRespone(BeaconResponseMessage beaconReasponse){
        
        this.recvdBeacons.add(beaconReasponse);
        this.latestRecvdBeacon++;
    }

    @Override
    public String toString() {
        
        String outString = "";
        
        long currentTime = System.currentTimeMillis();    
        outString += "UIStats at "+new Date(currentTime)+"\n";
        outString += "1)Number of packets sent "+this.numberOfPacketsSent+"\n";
        outString += "2)Number of bytes sent "+this.numberOfBytesSent+"\n";
        outString += "3)Number of packets received "+this.numberOfPacketsReceived+"\n";
        outString += "4)Number of bytes received "+this.numberOfBytesReceived+"\n";
        outString += "5)Number of packets retransmitted "+this.numberOfPacketsRetransmitted+"\n";
        outString += "6)Number of bytes retransmitted "+this.numberOfBytesRetransmitted+"\n";
        if(this.latestReceivedMessagTime != 0){
            outString += "*)Last received message at "+new Date(this.latestReceivedMessagTime)+"\n";
        }
        if(this.startTransmissionTime != 0){
            outString += "*)Total time taken so far "+(((currentTime - this.startTransmissionTime)*1.0)/1000)+" seconds";
        }    
        
        return outString;

    }    
   
}
