/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.stats;

import edu.helios.common.RobotCommand;
import edu.helios.exception.HeliosException;
import edu.helios.message.UplinkMessage;

/**
 *
 * @author Shreyas
 */
public class ReceivedRecord {

    
    private int sequenceNumber = 0;
    
    private RobotCommand command = null;
    
    private UplinkMessage receivedMessage = null;
    
    public ReceivedRecord(int seqNumber, RobotCommand command, UplinkMessage recvdMess) throws HeliosException{

        if(command == null){
            throw new HeliosException("The passed command to the constructor of ReceivedRecord is null");
        }
        if(recvdMess == null){
            throw new HeliosException("The passed received message to the constructor of ReceivedRecord is null");
        }
        if(seqNumber < 0){
            throw new HeliosException("The passed seqnumber "+seqNumber+" the constructor of ReceivedRecord is invalid");
        }
        
        this.sequenceNumber = seqNumber;
        this.command = command;
        this.receivedMessage = recvdMess;
    }

    public RobotCommand getCommand() {
        return command;
    }

    public UplinkMessage getReceivedMessage() {
        return receivedMessage;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }
    
    
    
    
    
}
