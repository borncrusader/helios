/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.message;

import edu.helios.common.HeliosConstants;
import edu.helios.exception.HeliosException;
import java.util.Vector;

/**
 * This class represents the beacon response message that is 
 * uni-casted from the robot back to the base station. The 
 * length field denotes the number of ID's that are included 
 * in the route. The route field is a vector that represents 
 * the ID's of all forwarders that have seen the request message.
 * 
 */

public class BeaconResponseMessage extends HeliosMessage {

    private int length;
    private Vector route = null;

    public BeaconResponseMessage(int seqNumber) throws HeliosException {

        super(HeliosConstants.BEACON_RESPONSE_TYPE, seqNumber);
    }

    public void appendToRoute(int nodeId) {

        Integer node = new Integer(nodeId);
        if (this.route == null) {
            this.route = new Vector();
        }
        route.addElement(node);
        this.length++;

    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Vector getRoute() {
        return route;
    }

    public void setRoute(Vector route) {
        this.route = route;
    }
    
    public String toString() {
        
        String superString = super.toString();
        return superString+ ", length=" + length + ", route=" + route;
    }
}
