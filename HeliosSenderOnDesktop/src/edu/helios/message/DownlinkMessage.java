/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.message;

import edu.helios.common.HeliosConstants;
import edu.helios.common.RobotCommand;
import edu.helios.exception.HeliosException;

/**
 * This class represents the downlink message from
 * the base station to the sun spot mounted on the 
 * robot. The command denotes the command that the
 * robot has to execute.
 */
public class DownlinkMessage extends HeliosMessage{
    
    private RobotCommand command = null;
    
    public DownlinkMessage(int sequenceNumber, RobotCommand command) throws HeliosException{
        
        super(HeliosConstants.DOWNLINK_MESS_TYPE ,sequenceNumber);
        if(command == null){
            throw new HeliosException("The passed command to the comstructor is null");
        }
        this.command = command;
        
    }

    public String toString() {
        
        String superString = super.toString();
        return superString + ", command=" + command;
    }

    
    
    public RobotCommand getCommand() {
        return command;
    }

    public void setCommand(RobotCommand command) {
        this.command = command;
    }
    
    
       
    
}
