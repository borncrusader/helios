package edu.helios.receiver;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
import com.sun.spot.core.peripheral.Spot;
import com.sun.spot.core.resources.Resources;
import com.sun.spot.core.resources.transducers.ITriColorLED;
import com.sun.spot.core.util.Utils;
import com.sun.spot.espot.peripheral.ESpot;
import com.sun.spot.ieee_802_15_4_radio.IRadioPolicyManager;
import com.sun.spot.multihop.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.multihop.radio.NoRouteException;
import edu.helios.common.HeliosConstants;
import edu.helios.message.BeaconRequestMessage;
import edu.helios.message.BeaconResponseMessage;
import edu.helios.message.HeliosMessage;
import edu.helios.message.UplinkMessage;
import edu.helios.utils.BeaconMessageUtils;
import edu.helios.utils.MessageUtils;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class BeaconResponseThread implements Runnable {

    /*
     * This class is a thread that listens for Beacon Request message on the 
     * port 80. When it receives it, it removes the ID's in it and adds it to
     * a Beacon response message. This message is unicasted back to the host 
     * application. When the host application receives the Beacon response 
     * message, it can extract the ID's to get the intermediate spots which 
     * have received the beacon request message thus getting the topology
     * info.
     */
    
    private int selfId = 0;
    private RadiogramConnection radioConnectionBroadcast = null;
    private RadiogramConnection radioConnectionReceive = null;
    private Datagram sendDatagram = null;
    private Datagram receiveDatagram = null;
    private int operationPort = 0;
    private int beaconReqSequenceNumber = 0;
    private String baseStationAddress = null;

    public BeaconResponseThread(int operationPort, String baseStatioAddress) {

        this.operationPort = operationPort;
        this.baseStationAddress = baseStatioAddress;


    }

    private void setMyId() {

        int index = 0;
        String lastFourDigits = "";
        String ourAddress = System.getProperty("IEEE_ADDRESS");
        index = ourAddress.lastIndexOf('.');
        lastFourDigits = ourAddress.substring(index + 1);
        selfId = Integer.parseInt(lastFourDigits, 16);

    }

    public void run() {
        HeliosMessage recvdMessage = null;
        BeaconMessageUtils messUtil = new BeaconMessageUtils();
        setMyId();
        String ourAddress = System.getProperty("IEEE_ADDRESS");
        ITriColorLED led = (ITriColorLED) Resources.lookup(ITriColorLED.class, "LED7");
        System.out.println("BEACON RESPONSE : Starting sensor sampler application on " + ourAddress + " ...");
        IRadioPolicyManager rpm = ((ESpot) Spot.getInstance()).getRadioPolicyManager();
        rpm.setOutputPower(HeliosConstants.OUTPUT_POWER);

        try {
            /*
             * Initialize the connection.
             */
            radioConnectionBroadcast = (RadiogramConnection) Connector.open("radiogram://" + this.baseStationAddress + ":" + this.operationPort);
            sendDatagram = radioConnectionBroadcast.newDatagram(HeliosConstants.MAX_DATAGRAM_LENGTH);
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            radioConnectionReceive = (RadiogramConnection) Connector.open("radiogram://:" + this.operationPort);
            receiveDatagram = radioConnectionReceive.newDatagram(radioConnectionReceive.getMaximumLength());
            // Open up a broadcast connection to the host port
            // where the 'on Desktop' portion of this demo is listening
        } catch (Exception e) {
            System.err.println("BEACON RESPONSE : Caught " + e + " in connection initialization.");
            //notifyDestroyed();
        }

        while (true) {
            try {

                /*
                 * Wait for a beacon request message.
                 */
                System.out.println("BEACON RESPONSE : Waiting for messages.....");

                radioConnectionReceive.receive(receiveDatagram);
                /*
                 * Decode the received packet
                 */
                recvdMessage = messUtil.decodePacket(receiveDatagram);
                System.out.println("BEACON RESPONSE : Received the packet : " + recvdMessage);

                if (recvdMessage.getType() == HeliosConstants.BEACON_REQUEST_TYPE) {
                    if (this.beaconReqSequenceNumber < recvdMessage.getSeqNumber()) {
                        /*
                         * Check if the received beacon request message is not a duplicate
                         */
                        System.out.println("BEACON RESPONSE : Got the beacon request mess : " + recvdMessage);
                        //((BeaconRequestMessage) recvdMessage).appendToRoute(selfId);
                        this.beaconReqSequenceNumber = recvdMessage.getSeqNumber();
                    } else {
                        /*
                         * Received a duplicate message. Discard and do nothing
                         */
                        System.out.println("BEACON RESPONSE : Already seen the message. Doing nothing. Mess : " + recvdMessage);
                        receiveDatagram.reset();
                        continue;
                    }
                } else {
                    /*
                     * Received a message of an unexpected type. DIscard and do nothing.
                     */
                    System.out.println("BEACON RESPONSE : Received an unknown mess. Doing nothing. Mess : " + recvdMessage);
                    receiveDatagram.reset();
                    continue;
                }

                /*
                 * Get the route ID's from the beacon request message
                 */
                BeaconRequestMessage beaconRequest = (BeaconRequestMessage) recvdMessage;
                BeaconResponseMessage beaconResponse = new BeaconResponseMessage(beaconRequest.getSeqNumber());
                /*
                 * Set the route ID's in the beacon response message
                 */
                beaconResponse.setLength(beaconRequest.getLength());
                beaconResponse.setRoute(beaconRequest.getRoute());

                /*
                 * Encode the packet and unicast it back to the host
                 */
                messUtil.encodePacket(beaconResponse, sendDatagram);

                System.out.println("BEACON RESPONSE : Forwarding the message : " + beaconResponse);
                while (true) {
                     /*
                      * Unicast the data in a loop till the packet is sent.
                      * The send method can send a "NoRouteException" because
                      * the underlying protocol that sends the datagram is the
                      * default wireles mesh throws it. 
                      */
                    try {
                        radioConnectionBroadcast.send(sendDatagram);
                        break;
                    } catch (NoRouteException e) {
                       /*
                        * Got a "NoRouteException". Try to send it again and
                        * hopefully the robot has moved in the range of a 
                        * forwarder
                        */
                        System.out.println("got exception : " + e.getMessage());
                        continue;
                    }
                }

                sendDatagram.reset();
                receiveDatagram.reset();
            } catch (Exception e) {
                System.err.println("BEACON RESPONSE : Caught " + e + " while collecting/sending sensor sample.");
            }
        }
    }
}
