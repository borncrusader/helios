    /*
 * StartApplication.java
 *
 * Created on Jul 16, 2010 12:17:23 AM;
 */

package org.sunspotworld;

import com.sun.spot.core.resources.Resources;
import com.sun.spot.core.resources.transducers.IIOPin;
import com.sun.spot.core.resources.transducers.ISwitch;
import com.sun.spot.core.resources.transducers.ITriColorLED;
import com.sun.spot.core.util.Utils;
import com.sun.spot.edemo.EDemoBoard;
import com.sun.spot.espot.service.BootloaderListenerService;
import com.sun.spot.ieee_802_15_4_radio.IRadioPolicyManager;
import com.sun.spot.ieee_802_15_4_radio.util.IEEEAddress;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;


/**
 * The startApp method of this class is called by the VM to start the
 * application.
 * 
 * The manifest specifies this class as MIDlet-1, which means it will
 * be selected for execution.
 */
public class StartApplication extends MIDlet {

    private ITriColorLED [] leds = EDemoBoard.getInstance().getLEDs();
    private IIOPin input = EDemoBoard.getInstance().getIOPins()[EDemoBoard.D0];
    private static final int MAX_SONAR_CONVERSION_FACTOR = 147; //per inch from white paper for this sensor

    protected void startApp() throws MIDletStateChangeException {
        System.out.println("Hello, world");
        BootloaderListenerService.getInstance().start();   // monitor the USB (if connected) and recognize commands from host

        long ourAddr = ((IRadioPolicyManager) Resources.lookup(IRadioPolicyManager.class)).getIEEEAddress();
        System.out.println("Our radio address = " + IEEEAddress.toDottedHex(ourAddr));

        int pulse;
        ISwitch sw1 = EDemoBoard.getInstance().getSwitches()[EDemoBoard.SW1];
        leds[0].setRGB(100,0,0);                // set color to moderate red
        while (sw1.isOpen()) {                  // done when switch is pressed
            leds[0].setOn();                    // Blink LED
            pulse = EDemoBoard.getInstance().getPulse(input,true,100);
            System.out.println("Pulse from Ultrasound: " + pulse);
            System.out.println(pulse/MAX_SONAR_CONVERSION_FACTOR + " inches");
            Utils.sleep(250);                   // wait 1/4 seconds
            leds[0].setOff();
            Utils.sleep(1000);                  // wait 1 second
        }
        notifyDestroyed();                      // cause the MIDlet to exit
    }

    protected void pauseApp() {
        // This is not currently called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * I.e. if startApp throws any exception other than MIDletStateChangeException,
     * if the isolate running the MIDlet is killed with Isolate.exit(), or
     * if VM.stopVM() is called.
     * 
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true when this method is called, the MIDlet must
     *    cleanup and release all resources. If false the MIDlet may throw
     *    MIDletStateChangeException  to indicate it does not want to be destroyed
     *    at this time.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        for (int i = 0; i < 8; i++) {
            leds[i].setOff();
        }
    }
}
