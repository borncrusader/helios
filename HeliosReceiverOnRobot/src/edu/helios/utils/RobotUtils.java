/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.helios.utils;

import com.sun.spot.core.util.Utils;
import com.sun.spot.edemo.EDemoBoard;
import com.sun.spot.edemo.peripheral.Servo;
import edu.helios.common.HeliosConstants;
import edu.helios.common.RobotCommand;

/*
 * This class is an utility class that is used to
 * execute the robot commands to be sent by the base 
 * station 
 */
public class RobotUtils {
    private final int HALF_RANGE = 500; 
    private Servo[] wheels = {new Servo(EDemoBoard.getInstance().getOutputPins()[0]), new Servo(EDemoBoard.getInstance().getOutputPins()[1])};
    private int [] stop = new int [2];

    private boolean servoActive = false;
    
    public RobotUtils() {
        stop[0] = 1463;
        stop[1] = 1463;
        
        for ( int i=0; i<2; i++) {
             wheels[i].setBounds(stop[i]-HALF_RANGE,stop[i]+HALF_RANGE);
        }
    }
    
    private void brake(long ms) {
        servoActive = true;
        for(int i=0; i<2; i++) {
            wheels[i].setPosition((float).5);  //position .5 corresponds to the stop value above
        }
        Utils.sleep(ms);
        servoActive = false;
    }
    
    private void stop() {
        brake(100);
        for(int i=0; i<2; i++) {
            wheels[i].setValue(0);  //set servo pins low
        }
    }

    private void moveForward(long ms) {
        servoActive = true;
        wheels[0].setPosition(0);
        wheels[1].setPosition(1);
        Utils.sleep(ms);
        servoActive = false;
    }
    
    private void moveReverse(long ms) {
        servoActive = true;
        wheels[0].setPosition(1);
        wheels[1].setPosition(0);
        Utils.sleep(ms);
        servoActive = false;
    }
    
    private void moveLeft(long ms) {
        servoActive = true;
        wheels[0].setPosition(1);
        wheels[1].setPosition(1);
        Utils.sleep(ms);
        servoActive = false;
    }
    
    private void moveRight(long ms) {
        servoActive = true;
        wheels[0].setPosition(0);
        wheels[1].setPosition(0);
        Utils.sleep(ms);
        servoActive = false;
    }
        
    public boolean isServoActive() {
        return servoActive;
    }
    
    public void executeCommand(RobotCommand command){
        if (command.getCommandType() == HeliosConstants.MOVE_FORWARD_CMD) {
            moveForward(command.getDuration());
        } else if (command.getCommandType() == HeliosConstants.MOVE_REVERSE_CMD) {
            moveReverse(command.getDuration());
        } else if (command.getCommandType() == HeliosConstants.MOVE_LEFT_CMD) {
            moveLeft(command.getDuration());
        } else if (command.getCommandType() == HeliosConstants.MOVE_RIGHT_CMD) {
            moveRight(command.getDuration());
        }
        stop();
    }
}
