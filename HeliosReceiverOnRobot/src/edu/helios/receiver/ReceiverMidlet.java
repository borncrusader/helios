package edu.helios.receiver;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.sun.spot.core.peripheral.IDriver;
import com.sun.spot.core.peripheral.Spot;
import com.sun.spot.espot.peripheral.ESpot;
import com.sun.spot.core.resources.Resources;
import com.sun.spot.core.resources.transducers.ILightSensor;
import com.sun.spot.core.resources.transducers.ITriColorLED;
import com.sun.spot.core.util.Utils;
import com.sun.spot.ieee_802_15_4_radio.IRadioPolicyManager;
import com.sun.spot.multihop.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.multihop.radio.NoRouteException;
import edu.helios.common.HeliosConstants;
import edu.helios.message.DownlinkMessage;
import edu.helios.message.HeliosMessage;
import edu.helios.message.UplinkMessage;
import edu.helios.utils.MessageUtils;
import edu.helios.utils.RobotUtils;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * This class is is the main application that runs on the sun spots
 * of the robots. It waits for instructions from the host and runs 
 * the command. The light value is recorded at the end of the command
 * execution and is unicasted back to the sender in an Uplink message.
 * 
 * This also starts a thread to handle beacon requests and unicast the 
 * beacon response back so that the host application has the topology
 * info.
 */
public class ReceiverMidlet extends MIDlet implements IDriver {
    
    private RadiogramConnection radioConnectionBroadcast = null;
    
    private RadiogramConnection radioConnectionReceive = null;
    
    private Datagram sendDatagram = null;
    
    private Datagram receiveDatagram = null;
    
    private RobotUtils rUtils = null; // not a member var because rUtils.isServoActive is required by the tearDown function
    
    private int latestRecvdSeqNumber = 0;
    
    protected void startApp() throws MIDletStateChangeException {
        UplinkMessage uplinkMessage = null;
        HeliosMessage recvdMessage = null;
        DownlinkMessage downLinkMessage = null;
        MessageUtils messUtil = new MessageUtils();
        String ourAddress = System.getProperty("IEEE_ADDRESS");
        
        IRadioPolicyManager rpm = ((ESpot)Spot.getInstance()).getRadioPolicyManager();
        rpm.setOutputPower(HeliosConstants.OUTPUT_POWER);
        int reading = 0;
        //int seqNo = 1;
        ILightSensor lightSensor = (ILightSensor)Resources.lookup(ILightSensor.class);
        ITriColorLED led = (ITriColorLED)Resources.lookup(ITriColorLED.class, "LED7");
        System.out.println("Starting sensor sampler application on " + ourAddress + " ...");
        
        Spot.getInstance().getDriverRegistry().add(this);  //have the SPOT check with us before going to deep sleep
        rUtils = new RobotUtils();

        try {
            /*
             * Initialize the connections
             */   
            radioConnectionBroadcast = (RadiogramConnection) Connector.open("radiogram://" + HeliosConstants.BASE_STATION_ADDRESS + ":" + HeliosConstants.SEND_PORT);
            sendDatagram = radioConnectionBroadcast.newDatagram(HeliosConstants.MAX_DATAGRAM_LENGTH);
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            radioConnectionReceive = (RadiogramConnection) Connector.open("radiogram://:" + HeliosConstants.RECEIVE_PORT);
            receiveDatagram = radioConnectionReceive.newDatagram(radioConnectionReceive.getMaximumLength());
            // Open up a broadcast connection to the host port
            // where the 'on Desktop' portion of this demo is listening
            try{
                /*
                 * Start a thread to listen for beacon request messages and send the 
                 * beacon response messages back to the host.
                 */
                Thread beaconResponseThread = new Thread(new BeaconResponseThread(HeliosConstants.BEACON_PORT_NUMBER, HeliosConstants.BASE_STATION_ADDRESS));
                beaconResponseThread.start();
            }catch (Exception ex){
                System.out.println("Caughtan exception while initialising the beacon response thread : "+ex.getMessage());
            }

        } catch (Exception e) {
            System.err.println("Caught " + e + " in connection initialization.");
            notifyDestroyed();
        }
        
        while (true) {
            try {
               
                led.setRGB(255, 0, 0);
                led.setOn();
                /*
                 * Wait for a Downlink message.
                 */
                System.out.println("Waiting for instructions.....");
                /*
                 * Decode the received packet
                 */
                radioConnectionReceive.receive(receiveDatagram);
                recvdMessage = messUtil.decodePacket(receiveDatagram);
                System.out.println("Received the packet : "+recvdMessage);
                if(recvdMessage.getType() != HeliosConstants.DOWNLINK_MESS_TYPE){
                    /*
                     * The received message is of an unexpected type. Discard and do nothing
                     */
                    System.out.println("Received an unexpected message.. Doing nothing.. Mess : "+recvdMessage);
                    continue;
                }
                downLinkMessage = (DownlinkMessage) recvdMessage;
                if(downLinkMessage.getSeqNumber() <= latestRecvdSeqNumber){
                    /*
                     * Got a duplicate message. Send an implicit ack by retransmitting the
                     * previous uplink message sent.
                     */
                    System.out.println("Retransmitting the previous message : "+uplinkMessage);
                    sendDatagram.reset();
                    messUtil.encodePacket(uplinkMessage, sendDatagram);
                    while (true) {
                       /*
                        * Unicast the data in a loop till the packet is sent.
                        * The send method can send a "NoRouteException" because
                        * the underlying protocol that sends the datagram is the
                        * default wireles mesh throws it. 
                        */
                        try {
                            radioConnectionBroadcast.send(sendDatagram);
                            break;
                        } catch (NoRouteException e) {
                           /*
                            * Got a "NoRouteException". Try to send it again and
                            * hopefully the robot has moved in the range of a 
                            * forwarder
                            */
                            System.out.println("got exception : " + e.getMessage());
                            continue;
                        }
                    }
                    
                    continue;
                }
                
                 // Get the current time and sensor reading
                //long now = System.currentTimeMillis();
  
                // Flash an LED to indicate a sampling event
                led.setRGB(255, 255, 255);
                led.setOn();
                Utils.sleep(50);
                led.setOff();
                
                /*
                 * Execute the passed comand
                 */
                rUtils.executeCommand(downLinkMessage.getCommand());
                
                /*
                 * Get the light value from the sensor
                 */
                reading = lightSensor.getValue();
                
                /*
                 * Package the value into an uplink message and unicast it back to
                 * host
                 */
                
                if(uplinkMessage == null){
                   uplinkMessage = new UplinkMessage(downLinkMessage.getSeqNumber(), reading);
                }
                
                //uplinkMessage = new UplinkMessage(seqNo, reading);
                //seqNo++;
                uplinkMessage.setValue(reading);
                uplinkMessage.setSeqNumber(downLinkMessage.getSeqNumber());
                latestRecvdSeqNumber = downLinkMessage.getSeqNumber();
                messUtil.encodePacket(uplinkMessage, sendDatagram);
                System.out.println("Sending a message : "+uplinkMessage);
                System.out.println("Light value = " + reading);
                //dg.writeLong(now);
                //dg.writeInt(reading);
                while (true) {
                    /*
                     * Unicast the data in a loop till the packet is sent.
                     * The send method can send a "NoRouteException" because
                     * the underlying protocol that sends the datagram is the
                     * default wireles mesh throws it. 
                     */
                    try {
                        radioConnectionBroadcast.send(sendDatagram);
                        break;
                    } catch (NoRouteException e) {
                        /*
                         * Got a "NoRouteException". Try to send it again and
                         * hopefully the robot has moved in the range of a 
                         * forwarder
                         */
                        System.out.println("got exception : " + e.getMessage());
                        continue;
                    }
                }

                // Go to sleep to conserve battery
                //Utils.sleep(HeliosConstants.SAMPLE_PERIOD - (System.currentTimeMillis() - now));
                sendDatagram.reset();
                receiveDatagram.reset();
            } catch (Exception e) {
                System.err.println("Caught " + e + " while collecting/sending sensor sample.");
            }
        }
    }
    
    //
    //IDriver methods
    //
    /**
     * Implements getDriverName of IDriver interface
     * @return the name of this Driver 
     */
    public String getDriverName() {
        return "Helios";
    }
    
    /**
     * Check if robot is ready for SPOT to deep sleep
     * @return whether the servos need to sustain a pulse
     */
    public boolean tearDown() {
        return !rUtils.isServoActive();
    }

    /**
     * what to do when SPOT comes out of deep sleep
     */
    public void setUp() {
        //do nothing
    }
    
    /**
     * what to do when SPOT shuts down
     */
    public void shutDown() {
        //do nothing
    }
        
    protected void pauseApp() {
        // This will never be called by the Squawk VM
    }
    
    protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
        // Only called if startApp throws any exception other than MIDletStateChangeException
    }
    
}
