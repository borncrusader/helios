/*
 * SendDataDemoHostApplication.java
 *
 * Copyright (c) 2008-2009 Sun Microsystems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package org.sunspotworld.demo;

//import com.sun.spot.io.j2me.radiogram.*;
//import com.sun.spot.peripheral.ota.OTACommandServer;

import com.sun.spot.espot_host.ota.OTACommandServer;
import com.sun.spot.multihop.io.j2me.radiogram.RadiogramConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.text.DateFormat;
import java.util.Date;
import javax.microedition.io.*;
import com.sun.spot.core.util.Utils;


/**
 * This application is the 'on Desktop' portion of the SendDataDemo. 
 * This host application collects sensor samples sent by the 'on SPOT'
 * portion running on neighboring SPOTs and just prints them out. 
 *   
 * @author: Vipul Gupta
 * modified: Ron Goldman
 */
public class SendDataDemoHostApplication {
    // Broadcast port on which we listen for sensor samples
    private static final int HOST_PORT = 67;
    private static final int UPLINK_MSG = 0;
    private static final int DOWNLINK_MSG = 1;
    private static final int FORWARD = 0;
    private static final int REVERSE = 1;
    private static final int LEFT = 2;
    private static final int RIGHT = 3;
        
    private void run() throws Exception {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(inputStreamReader);
        
        RadiogramConnection rCon;
        Datagram dg;
        DateFormat fmt = DateFormat.getTimeInstance();
         
        try {
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            rCon = (RadiogramConnection) Connector.open("radiogram://broadcast:" + HOST_PORT);
            dg = rCon.newDatagram(rCon.getMaximumLength());
            System.out.println(rCon.getMaximumLength());
        } catch (Exception e) {
             System.err.println("setUp caught " + e.getMessage());
             throw e;
        }

        
        // Main data collection loop
        while (true) {
            try {
                System.out.println("command:");
                String cmd = reader.readLine();
                
                int command;
                
                int seqno = 0;
                int msg_type = DOWNLINK_MSG;
                
                if (cmd.equalsIgnoreCase("forward")) {
                    command = FORWARD;
                } else if (cmd.equalsIgnoreCase("reverse")) {
                    command = REVERSE;
                } else if (cmd.equalsIgnoreCase("left")) {
                    command = LEFT;
                } else if (cmd.equalsIgnoreCase("right")) {
                    command = RIGHT;
                } else {
                    continue;
                }
                
                System.out.println("time:");
                cmd = reader.readLine();
                
                int time = Integer.parseInt(cmd);
                
                dg.writeInt(msg_type);
                dg.writeInt(seqno);
                dg.writeInt(command);
                dg.writeInt(time);
                rCon.send(dg);
                dg.reset();
                
            } catch (Exception e) {
                System.err.println("Caught " + e +  " while reading sensor samples.");
                throw e;
            }
        }
        
    }
    
    /**
     * Start up the host application.
     *
     * @param args any command line arguments
     */
    public static void main(String[] args) throws Exception {
        // register the application's name with the OTA Command server & start OTA running
        OTACommandServer.start("LightSeeker-OnDesktop");

        SendDataDemoHostApplication app = new SendDataDemoHostApplication();
        app.run();
    }
}
