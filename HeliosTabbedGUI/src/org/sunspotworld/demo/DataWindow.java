/*
 * DataWindow.java
 *
 * Copyright (c) 2008 Sun Microsystems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package org.sunspotworld.demo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;

import java.text.DateFormat;
import java.util.Date;
import javax.swing.*;

/**
 * Create a new window to graph the sensor readings in.
 *
 * @author Ron Goldman
 */
public class DataWindow extends JFrame {
    // Variables declaration - do not modify                                    
    private javax.swing.JTabbedPane heliosTabPane;
    private javax.swing.JPanel statsTabPane;
    private javax.swing.JPanel plotTabPane;
    private javax.swing.JPanel pathTabPane;
    private javax.swing.JPanel topoTabPane;
    private javax.swing.JScrollPane plotScrollPane;
    private javax.swing.JTextPane plotTextArea;
    private javax.swing.JScrollPane statsScrollPane;
    private javax.swing.JTextPane statsTextArea;
    private javax.swing.JScrollPane pathScrollPane;
    private javax.swing.JTextPane pathTextArea;
    private javax.swing.JScrollPane topoScrollPane;
    private javax.swing.JTextPane topoTextArea;
    // End of variables declaration   
           
    private static final int MAX_SAMPLES = 10000;
    private int index = 0;
    private long[] time = new long[MAX_SAMPLES];
    private int[] val = new int[MAX_SAMPLES];
    DateFormat fmt = DateFormat.getDateTimeInstance();

    /** Creates new form DataWindow */
    public DataWindow() {
        initComponents();
    }

    public DataWindow(String title) {
        initComponents();
        setTitle(title);
    }
    
    public JTabbedPane createTabMenu(){
        //Create the tab pane
        heliosTabPane = new JTabbedPane();
        
        //Build Statistics tab
        statsTabPane = new JPanel(new BorderLayout());
        statsTabPane.setOpaque(true);
        //Create text area
        statsTextArea = new JTextPane();
        statsTextArea.setEditable(false);
        //Add scrolled text area 
        statsScrollPane = new JScrollPane(statsTextArea);        
        //Add text area to stats pane
        statsTabPane.add(statsScrollPane);        
        heliosTabPane.add("Statistics",statsTabPane);
      
        //Build Plot tab
        plotTabPane = new JPanel(new BorderLayout());
        plotTabPane.setOpaque(true);
        //Create text area
        plotTextArea = new JTextPane();
        plotTextArea.setEditable(false);
        //Add scrolled text area 
        plotScrollPane = new JScrollPane(plotTextArea);
        //Add text area to plot pane
        plotTabPane.add(plotScrollPane);
        heliosTabPane.add("Plot",plotTabPane);
        
        //Build Path tab
        pathTabPane = new JPanel(new BorderLayout());
        pathTabPane.setOpaque(true);
        //Create text area
        pathTextArea = new JTextPane();
        pathTextArea.setEditable(false);
        //Add scrolled text area 
        pathScrollPane = new JScrollPane(pathTextArea);
        //Add text area to content pane
        pathTabPane.add(pathScrollPane);
        heliosTabPane.add("Path",pathTabPane);
      
        //Build Topology tab
        topoTabPane = new JPanel(new BorderLayout());
        topoTabPane.setOpaque(true);
        //Create text area
        topoTextArea = new JTextPane();
        topoTextArea.setEditable(false);
        //Add scrolled text area 
        topoScrollPane = new JScrollPane(topoTextArea);
        //Add text area to Topology pane
        topoTabPane.add(topoScrollPane);
        heliosTabPane.add("Topology",topoTabPane);
        
        
        
        return heliosTabPane;
    }    
     
    /*
    public class GUIAction implements ActionListener{  
        public void actionPerformed(ActionEvent e){  
            String str = e.getActionCommand();  
            if(str.equals("Plot")){  
                
            }  
            else if(str.equals("Statistsics")){ 
                
  
            }  
        }  
    }
    */    
    
    
    /*
    public JPanel createContentPane() {
        //Create content pane
        JPanel contentPane = new JPanel(new BorderLayout());
        contentPane.setOpaque(true);
        //Create text area
        mainTextArea = new JTextArea(5,30);
        mainTextArea.setEditable(false);
        //Add scrolled text area 
        mainScrollPane = new JScrollPane(mainTextArea);
        //Add text area to content pane
        contentPane.add(mainScrollPane,BorderLayout.CENTER);
        
        return contentPane;        
    }
    */
    
    
    
    public void addData(long t, int v) {
        time[index] = t;
        val[index] = v;
        
        
        /*
        mainTextArea.append(fmt.format(new Date(t)) + "    value = " + v + "\n");
        mainTextArea.setCaretPosition(mainTextArea.getText().length());        
        //mainTextArea.setCaretPosition(0);
        */        
        repaint();
    }
    
    // Graph the sensor values in the dataPanel JPanel
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        /*
        int left = dataPanel.getX() + 10;       // get size of pane
        int top = dataPanel.getY() + 30;
        int right = left + dataPanel.getWidth() - 20;
        int bottom = top + dataPanel.getHeight() - 20;
        
        int y0 = bottom - 20;                   // leave some room for margins
        int yn = top;
        int x0 = left + 33;
        int xn = right;
        double vscale = (yn - y0) / 800.0;      // light values range from 0 to 800
        double tscale = 1.0 / 2000.0;           // 1 pixel = 2 seconds = 2000 milliseconds
        
        // draw X axis = time
        g.setColor(Color.BLACK);
        g.drawLine(x0, yn, x0, y0);
        g.drawLine(x0, y0, xn, y0);
        int tickInt = 60 / 2;
        for (int xt = x0 + tickInt; xt < xn; xt += tickInt) {   // tick every 1 minute
            g.drawLine(xt, y0 + 5, xt, y0 - 5);
            int min = (xt - x0) / (60 / 2);
            g.drawString(Integer.toString(min), xt - (min < 10 ? 3 : 7) , y0 + 20);
        }
        
        // draw Y axis = sensor reading
        g.setColor(Color.BLUE);
        for (int vt = 800; vt > 0; vt -= 200) {         // tick every 200
            int v = y0 + (int)(vt * vscale);
            g.drawLine(x0 - 5, v, x0 + 5, v);
            g.drawString(Integer.toString(vt), x0 - 38 , v + 5);
        }

        // graph sensor values
        int xp = -1;
        int vp = -1;
        for (int i = 0; i < index; i++) {
            int x = x0 + (int)((time[i] - time[0]) * tscale);
            int v = y0 + (int)(val[i] * vscale);
            if (xp > 0) {
                g.drawLine(xp, vp, x, v);
            }
            xp = x;
            vp = v;
        }        
        */
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

}
