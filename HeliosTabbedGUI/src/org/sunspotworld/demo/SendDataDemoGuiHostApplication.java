/*
 * DatabaseDemoHostApplication.java
 *
 * Copyright (c) 2008-2009 Sun Microsystems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package org.sunspotworld.demo;

import com.sun.spot.multihop.io.j2me.radiogram.Radiogram;
import com.sun.spot.multihop.io.j2me.radiogram.RadiogramConnection;
import java.awt.BorderLayout;
import javax.swing.JFrame;


/**
 * This application is the 'on Desktop' portion of the SendDataDemo. 
 * This host application collects sensor samples sent by the 'on SPOT'
 * portion running on neighboring SPOTs and graphs them in a window. 
 *   
 * @author Vipul Gupta
 * modified Ron Goldman
 */
public class SendDataDemoGuiHostApplication {
    // Broadcast port on which we listen for sensor samples
    private static final int HOST_PORT = 67;
    private static int initDone = 0;        
    private DataWindow dataWindow;    
    
    private void setup() {
        /*
        JFrame fr = new JFrame("HELIOS monitor Robot 1");
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        dataWindow = new DataWindow("HELIOS monitor 2");
        fr.add(dataWindow.createTabMenu(),BorderLayout.CENTER);        
        
        //Display the window                
        fr.setSize(360, 200);
        fr.validate();
        fr.setVisible(true);  
        * 
        */
        MonitorWindow mw = new MonitorWindow("Helios Monitor Robot 1");
        mw.setSize(360, 200);
        mw.validate();
        mw.setVisible(true);
    }
    
    private DataWindow findPlot() {
        if(initDone == 0){
            java.awt.EventQueue.invokeLater(new Runnable() {            
                public void run() {                        
                    dataWindow.setVisible(true);                 
                }            
            });            
            initDone = 1;
            return dataWindow;
        }
        return dataWindow;
    }
           
    
    private void run() throws Exception {
        RadiogramConnection rCon;
        Radiogram dg;
        /*
        try {
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            rCon = (RadiogramConnection) Connector.open("radiogram://:" + HOST_PORT);
            dg = (Radiogram)rCon.newDatagram(rCon.getMaximumLength());
        } catch (Exception e) {
             System.err.println("setUp caught " + e.getMessage());
             throw e;
        }

        // Main data collection loop
        while (true) {
            try {
                // Read sensor sample received over the radio
                rCon.receive(dg);
                DataWindow dw = findPlot();
                long time = dg.readLong();      // read time of the reading
                int val = dg.readInt();         // read the sensor value
                dw.addData(time, val);
            } catch (Exception e) {
                System.err.println("Caught " + e +  " while reading sensor samples.");
                throw e;
            }
        } 
        */
        
        DataWindow dw = findPlot();
        
        while (true){
            dw.addData(1,23);
        }
      
    }
    
    /**
     * Start up the host application.
     *
     * @param args any command line arguments
     */
    public static void main(String[] args) throws Exception {
        // register the application's name with the OTA Command server & start OTA running
        //OTACommandServer.start("SendDataDemo-GUI");

        SendDataDemoGuiHostApplication app = new SendDataDemoGuiHostApplication();
        app.setup();
       // app.run();
    }
}
